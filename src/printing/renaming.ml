let pretty_string : (Str.regexp * string) list -> string -> string =
  fun iso s ->
  let rec aux l s = match l with
    | [] -> s
    | (pattern, new_s)::t -> aux t (Str.global_replace pattern new_s s)
  in
  aux iso s

let string_symbol_isomorphism =
  [ ({|-\.|}, "not") ; ({|->|}, "arrow") ; ({||->|}, "mapArrow")
  ; ({|\.|}, "Dot") ; ({|#|}, "Diese") ; ({|:|}, "Colon")
  ; ({|;|}, "Semicolon") ; ({|,|}, "Comma") ; ({|^@|}, "at")
  ; ({||-|}, "taquet") ; ({|||}, "Pipe") ; ({|\"|}, "Guillemet")
  ; ({|`|}, "Quote") ; ({|'|}, "SimpGuil")
  ; ({|^_$|}, "Underscore") ; ({|\[|}, "SqBrL") ; ({|\]|}, "SqBrR")
  ; ({|{|}, "CurBrL") ; ({|}|}, "CurBrR") ; ({|(|}, "PL") ; ({|)|}, "PR")
  ; ({|/\\|}, "land") ; ({|\\/|}, "or") ; ({|/|}, "slash")
  ; ({|\\|}, "backslash") ; ({|\^|}, "Chevron")
  ; ({|>|}, "Gt") ; ({|<|}, "Lt")
  ; ({|^\+|}, "Plus") ; ({|^-|}, "Tiret") ; ({|=|}, "eq")
  ; ({|^off$|}, "Off") ; ({|^prefix$|}, "prefixe") ; ({|^type$|}, "MM_type")
  ; ({|^induction$|}, "Induction_Peano") ]
  (* if x = "/_\\" then "and1" else
     if x = "_/_\\" then "and2" else
     if x = "_/_\\^n" then "and3" else *)

let string_symbol_isomorphism =
  List.map (fun (p,s) -> (Str.regexp p, s)) string_symbol_isomorphism

let name s = pretty_string string_symbol_isomorphism s
