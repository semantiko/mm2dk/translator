open MM_common.C_printer_file
open MM_common.Error
open N_types

let pp_na (f_l, d_l, e_l, s) b =
  pp _STDOUT "${\n" ;
  List.iter (fun x -> pp _STDOUT "  " ; pp_floating  x) f_l ;
  List.iter (fun x -> pp _STDOUT "  " ; pp_disjoint  x) d_l ;
  List.iter (fun x -> pp _STDOUT "  " ; pp_essential x) e_l ;
  pp _STDOUT "  " ; pp_statement s b

let pp_n_assert na = (match na with
  | SynAxiom na -> pp_na na true
  | SemAxiom na -> pp_na na true
  | Proof(na,p) -> (pp_na na false ; pp_proof p)) ; pp _STDOUT "$.\n$}\n"

let pp_n_file (c_ty, c_pr, c_op, va, pre_n) =
  let f_cst c = pp _STDOUT " %s" c in
  let f_var (v,_) = pp _STDOUT " %s" v in
  pp _STDOUT "$( Typing symbol(s) $)\n"  ;
  pp _STDOUT "$c" ; List.iter f_cst c_ty ; pp _STDOUT " $.\n" ;
  pp _STDOUT "$( Special symbol(s) $)\n" ;
  pp _STDOUT "$c" ; List.iter f_cst c_pr ; pp _STDOUT " $.\n" ;
  pp _STDOUT "$( Operator(s) $)\n" ;
  pp _STDOUT "$c" ; List.iter f_cst c_op ; pp _STDOUT " $.\n" ;
  pp _STDOUT "$( Variable(s) $)\n" ;
  pp _STDOUT "$v" ; List.iter f_var va   ; pp _STDOUT " $.\n" ;
  let f n_command = match n_command with
    | NImpo  i  -> pp_import   i
    | NVar   v  -> pp_variable v
    | NAsser na -> pp_n_assert na
  in
  List.iter f pre_n
