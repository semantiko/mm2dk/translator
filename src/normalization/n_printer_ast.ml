open MM_common.C_types
open MM_common.C_printer_ast
open N_types

(** Auxiliary Function to print a normalized parsed file as AST in Stdout *)

let pp_aux_n_assert tab a =
  print_sep_spec tab ;
  let fh () h = pp_aux_hypo (tab@["|\t"]) (Fhypothesis(h)) ; print_sep tab "|\n" in
  let fd () h = pp_aux_hypo (tab@["|\t"]) (Dhypothesis(h)) ; print_sep tab "|\n" in
  let fe () h = pp_aux_hypo (tab@["|\t"]) (Ehypothesis(h)) ; print_sep tab "|\n" in
  match a with
  | SynAxiom(fl, dl, el, asser) ->
     pp "n $syn_a\n" ; print_sep tab "|\n" ;
     List.fold_left fh () fl ; print_sep tab "|\n" ;
     List.fold_left fd () dl ; print_sep tab "|\n" ;
     List.fold_left fe () el ; print_sep tab "|\n" ;
     pp_aux_assert (tab@["|\t"]) (Aassertion(asser))
  | SemAxiom(fl, dl, el, asser) ->
     pp "n $sem_a\n" ; print_sep tab "|\n" ;
     List.fold_left fh () fl ; print_sep tab "|\n" ;
     List.fold_left fd () dl ; print_sep tab "|\n" ;
     List.fold_left fe () el ; print_sep tab "|\n" ;
     pp_aux_assert (tab@["|\t"]) (Aassertion(asser))
  | Proof((fl, dl, el, asser), proof) ->
     pp "n $p\n" ; print_sep tab "|\n" ;
     List.fold_left fh () fl ; print_sep tab "|\n" ;
     List.fold_left fd () dl ; print_sep tab "|\n" ;
     List.fold_left fe () el ; print_sep tab "|\n" ;
     pp_aux_assert (tab@["|\t"]) (Passertion(asser, proof))

let pp_n_command tab (n_command : n_command) =
  print_sep tab "\n" ;
  match n_command with
  | NImpo i ->
     print_sep_spec tab  ; pp "Import\n" ;
     print_sep tab "|\n" ; pp_aux_import (tab@["|\t"]) i
  | NVar v   -> pp_aux_decl tab (Vdeclaration(v))
  | NAsser s -> pp_aux_n_assert tab s

(** Function to print a normalized parsed file as AST in Stdout *)
let print_normalized_ast (c_ty, c_pr, c_op, va, pre_n) =
  let tab = ["|\t"] in
  pp "Normalized file\n" ;
  let f_cst c = pp "Symb(%s)" c in
  let f_var (v,_) = pp "Var(%s)" v in
  List.iter f_cst c_ty ;
  List.iter f_cst c_pr ;
  List.iter f_cst c_op ;
  List.iter f_var va   ;
  List.iter (pp_n_command tab) pre_n ; pp "EoF\n"
