open MM_common.C_types
open MM_common.Data_structure
open MM_common.Error
open Parsing.P_types
open N_types

let debug_symbol = ref false

(** [check_dh dh v_m] checks if every variable in the d-hypothesis [dh]
    has been declared in the file before, i.e. are in the variable map [v_m]. *)
let rec check_dh dh v_m = match dh with
  | DhypoOne(d1, d2) -> V_Decl.mem d1 v_m && V_Decl.mem d2 v_m
  | DhypoMany(t, d)  -> V_Decl.mem d  v_m && check_dh t v_m

(**This function transforms an assertion asser into a block that gathers needed hypotheses in hst, e_list and d_list set in correct order : and the assertion
   itself. The function also returns a new set that contains elements of cit_s (set of special symbols such as |-) plus eventual special symbols that
   can be found in the assertion *) (* TODO need to be readable *)
let build_n_assert asser fh_data eh_data dh_data cte_data type_cte_data special_cte_data =

  (* [eh_list_to_fh_map e_list fh_m] returns a list of fh_set's f-hypotheses,
    linked to the variables associated with the e-hypotheses list
    named [e_list], using the previous function. *) (* TODO fix renaming *)
  let eh_list_to_fh_map e_list fh_m =
    (* List.fold_left (fun res x -> eh_to_fh_map x fh_m res) M.empty e_list *)
    let rec aux e_list res = match e_list with
      | []   -> res
      | h::t -> aux t (eh_to_fh_map h fh_m res)
    in
    aux e_list M.empty
  in

  let new_fh_data = eh_list_to_fh_map eh_data fh_data in (*gives f hypotheses linked to the e hypotheses list, as well as a f hypotheses set that doesn
                                                                contains those in the previous list to avoid duplicates in the normalized assertion *)
  match asser with 
  (* If the assertion is an a_assertion. Depending of the first assert's typecode symbol, it could be a syntaxical or a sementical assertion *)
  | Aassertion((lab, typ, m)) ->
     if not(C_Decl.mem typ cte_data) then
       raise (MM2DKError(IllFormedMetamathFile, "normalization.ml", "build_n_assert",
                         typ ^ " in a_assertion labeled " ^ lab ^ " undefined in a constant declaration."))
     else
       let v_sem     x = SemAxiom(x, dh_data, eh_data, (lab, typ, m)) in
       let v_not_sem x = SynAxiom(x, dh_data, eh_data, (lab, typ, m)) in
       toto fh_data typ type_cte_data special_cte_data m new_fh_data v_sem v_not_sem
  (* If the assertion is a p assertion. We don't separate it like in the previous case but we still verify the typecode symbol of the assertion's start *)
  | Passertion((lab, typ, m), p_end) ->
     if not(C_Decl.mem typ cte_data) then
       raise (MM2DKError(IllFormedMetamathFile, "normalization.ml", "build_n_assert",
                         typ ^ " in p_assertion labeled " ^ lab ^ " undefined in a constant declaration."))
     else
       let v x = Proof((x, dh_data, eh_data, (lab,typ,m)) , p_end) in
       toto fh_data typ type_cte_data special_cte_data m new_fh_data v v

(**Function that transform a block into a list of normalized assertions, using the set of f hypotheses hst completed with previous elements of the main 
   file's block ; vmp the variables' map already declared, the set const_set of every c_declaration in the main block, the set of constants type_set that 
   represent types already declared in the main block, the set of main block's special constants cit_set, and the list of already generated d hypotheses.
   Thefunction also returns an updated type constants set and special constants set.
   . 
   Hypotheses on this function to make it work correctly :
   - no variable should be declared in a block, it has to be declared in the main file's block 
   - no variable already declared in a higher scope should be declared once again
   - no variable already typed in a higher scope's float hypothesis should be typed once again
   - If a new float hypothesis appears, its linked variable has to be declared in this block or in a higher scope
   - e_hypotheses and d_hypotheses in a block are considered well ordered before any assertion (no error we be returned otherwise and the 
     normalization processus may generate an untrusty result) *)
(* TODO need to be readable *)
let rec build_n_asser_block block fh_data counter var_data cte_data type_cte_data special_cte_data eh_data dh_data =
  let rec aux block fh_data counter var_data cte_data type_cte_data special_cte_data eh_data dh_data res = match block with
    | [] -> (res, type_cte_data, special_cte_data)
    | BComm(_)::t -> aux t fh_data counter var_data cte_data type_cte_data special_cte_data eh_data dh_data res
    | BVdecl(v)::t -> aux t fh_data counter (fill_v_map v var_data) cte_data type_cte_data special_cte_data eh_data dh_data res
    | BHypo(h)::t -> (
        match h with 
        | Dhypothesis(a) ->
           if check_dh a var_data
           then aux t fh_data counter var_data cte_data type_cte_data special_cte_data eh_data (dh_data@[a]) res
           else raise (MM2DKError(IllFormedMetamathFile, "normalization.ml", "build_n_asser_block", "Undeclared variables in a d-hypothesis."))
        | Ehypothesis((lab, typ, mms)) ->
           if (C_Decl.mem typ type_cte_data ) || typ = "(" || typ = ")"
           then aux t fh_data counter var_data cte_data type_cte_data special_cte_data (eh_data@[(lab, typ, mms)]) dh_data res
           else aux t fh_data counter var_data cte_data type_cte_data (C_Decl.add typ special_cte_data) (eh_data@[(lab, typ, mms)]) dh_data res
        | Fhypothesis((lab, typ, var)) ->
           if (V_Decl.mem var var_data) && (V_Decl.find var var_data = None) then
             let new_var_data = V_Decl.add var (Some(typ)) var_data in
             let new_type_cte_data = C_Decl.add typ type_cte_data in
             (* aux t (V_Decl.add lab (typ, var) fh_data) (counter+1) new_var_data cte_data new_type_cte_data special_cte_data eh_data dh_data res *)
             aux t (M.add counter (lab, typ, var) fh_data) (counter+1) new_var_data cte_data new_type_cte_data special_cte_data eh_data dh_data res
           else
             raise (MM2DKError(IllFormedMetamathFile, "normalization.ml", "build_n_asser_block", "Wrong instanciation of a f-hypothesis."))
      )
    | BAssert(h)::t ->
       let (na, more_cit_set) = build_n_assert h fh_data eh_data dh_data cte_data type_cte_data special_cte_data in
       let to_add = C_Decl.diff more_cit_set special_cte_data in
       let new_cit_set = C_Decl.union special_cte_data to_add in
       aux t fh_data counter var_data cte_data type_cte_data new_cit_set eh_data dh_data (na::res)
    | BBdecl(h)::t ->
       let (more_na, more_type_set, more_cit_set) = build_n_asser_block h fh_data counter var_data cte_data type_cte_data special_cte_data eh_data dh_data in
       aux t fh_data counter var_data cte_data more_type_set more_cit_set eh_data dh_data (more_na@res)
  in
  aux block fh_data counter var_data cte_data type_cte_data special_cte_data eh_data dh_data []


(**Transforms a parsed metamath file f in a pre normalized form as described in metamath_types.mli. The function also returns three sets that contain
   operator constants, type constants and special constants declared in the metamath file *)
(* TODO need to be readable *)
let pre_normalize_file file =
  (* [aux file cte_data type_cte_data special_cte_data var_data dh_data fh_data counter]
         - [file]
         - [cte_data] contains the constants,
         - [type_cte_data] contains type constants such as "term" or "wff",
         - [special_cte_data] contains special constants such as "|-",
         - [var_data] contains the variables,
         - [dh_data] contains the d-hypotheses,
         - [fh_data] contains the f-hypotheses,
         - [counter]
  *)
  let rec aux file cte_data type_cte_data special_cte_data var_data dh_data fh_data counter =
    match file with
    | [] ->
       if not (C_Decl.mem "(" cte_data && C_Decl.mem ")" cte_data) then
         wrn_msg _STDOUT "Warning: Parentheses aren't declared as constants. Errors might occur.";
       let new_cte_data = C_Decl.remove "(" (C_Decl.remove ")" cte_data) in
       let new_var_data = V_Decl.fold(fun key a res -> match a with None -> wrn_1 _STDOUT "MMFileError - Warning: Variable %s is untyped." key; res |Some(b) -> V_Decl.add key b res) var_data (V_Decl.empty) in
       (C_Decl.elements type_cte_data, C_Decl.elements special_cte_data, C_Decl.elements (C_Decl.diff (C_Decl.diff new_cte_data type_cte_data) special_cte_data), V_Decl.bindings new_var_data, fh_data), []
    (*After this operation, the initial set of undistincted constants only contains operator constants. Parentheses are also removed from it *)
    | Comm(_)::f -> aux f cte_data type_cte_data special_cte_data var_data dh_data fh_data counter
    | Impo(i)::f -> let res, t = aux f cte_data type_cte_data special_cte_data var_data dh_data fh_data counter in res, NImpo(i)::t
    | Decl(Cdeclaration(c))::f -> aux f (fill_c_set c cte_data) type_cte_data special_cte_data var_data dh_data fh_data counter
    | Decl(Vdeclaration(v))::f -> aux f cte_data type_cte_data special_cte_data (fill_v_map v var_data) dh_data fh_data counter
    | Hypo(h)::f -> (
        match h with
        | Ehypothesis(_) -> raise (MM2DKError(IllFormedMetamathFile, "normalization.ml", "pre_normalize_file", "e-hypothesis declaration in main block error."))
        | Dhypothesis(d) ->
           if not(check_dh d var_data) then
             raise (MM2DKError(IllFormedMetamathFile, "normalization.ml", "pre_normalize_file", "Undeclared variable in d-hypothesis error."))
           else aux f cte_data type_cte_data special_cte_data var_data (d::dh_data) fh_data counter
        | Fhypothesis((lab, typ, var)) ->
           if (V_Decl.mem var var_data) && (V_Decl.find var var_data = None) then
             let new_var_data = V_Decl.add var (Some(typ)) var_data in
             let new_type_cte_data = C_Decl.add typ type_cte_data in
             (* aux f cte_data new_type_cte_data special_cte_data new_var_data dh_data (V_Decl.add lab (typ, var) fh_data) (counter+1) *)
             aux f cte_data new_type_cte_data special_cte_data new_var_data dh_data (M.add counter (lab, typ, var) fh_data) (counter+1)
           else
             raise (MM2DKError(IllFormedMetamathFile, "normalisation.ml", "pre_normalize_file", "Wrong instanciation of a f-hypothesis.")))
    | Asser(a)::f ->
       let (na, new_special_cte_data) = build_n_assert a fh_data [] dh_data cte_data type_cte_data special_cte_data in
       let res, t = aux f cte_data type_cte_data new_special_cte_data var_data dh_data fh_data counter in res, NAsser(na)::t
    | Decl(Bdeclaration(b))::f ->
       let (na_l, new_type_cte_data, new_special_cte_data) = build_n_asser_block b fh_data counter var_data cte_data type_cte_data special_cte_data [] dh_data in
       (*Each assertions from the created list by build_n_assert_block are added to the in progress pre normalized file *)
       let rec aux2 l = match l with
         | []   -> aux f cte_data new_type_cte_data new_special_cte_data var_data dh_data fh_data counter
         | h::q -> let res, t = aux2 q in res, NAsser(h)::t
       in
       aux2 (List.rev na_l)
  in
  aux file C_Decl.empty C_Decl.empty C_Decl.empty V_Decl.empty [] M.empty 1

(**Transforms a set of f hypotheses into a map where keys are f hypotheses's labels and associated values are couples typecode*variable *)
(* TODO Need to be updated *)
let fh_map_ordered_to_fh_map_label fh_map = (* type more general btw *)
  let f _ (la, ty, va) res = V_Decl.add la (ty, va) res in
   M.fold f fh_map V_Decl.empty

(** Last step of the normalizing processus, using a pre normalized file f *)
(* TODO goal of this step? *)
let normalize_file file =
  let (c_ty, c_sp, c_op, var, fh), pre_n = pre_normalize_file file in

  if !debug_symbol then
    (let f s = pp _STDOUT " %s " s in
     Printf.printf "TYPES :" ;
     List.iter f c_ty ;
     Printf.printf "\nSPECIAL :" ;
     List.iter f c_sp ;
     Printf.printf "\nOP :" ;
     List.iter f c_op ;
     Printf.printf "\nVAR :" ;
     List.iter (fun (v,_) -> pp _STDOUT " %s " v) var  (* TODO Used only here *)
    ) ;
  (* The normalized file starts with three c-declarations that define every file's constants,
     and a v-declaration that defines its variables *)
  ((c_ty, c_sp, c_op, var, pre_n), fh_map_ordered_to_fh_map_label fh)
