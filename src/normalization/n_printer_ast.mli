open N_types
open MM_common.C_types

(** [print_normalized_ast c_ty, c_pr, c_op, va, pre_n]

    Function to print a normalized parsed file as AST in Stdout *)
val print_normalized_ast : c_decl * c_decl * c_decl * (math_symbol * string) list * n_file -> unit
