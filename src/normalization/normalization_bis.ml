open MM_common.C_types
open Parsing.P_types
(* open N_types *)

let update proj delta_list i =
  let rec aux l i = match l with
    | []   -> failwith "InternalError"
    | t::q ->
      if i = 0
      then (proj t)::q
      else t::(aux q (i-1))
  in
  aux delta_list i

let update_var l i x = update (fun (v, f, d, e) -> (v @ x, f, d, e)) l i
let update_fh  l i x = update (fun (v, f, d, e) -> (v, f @ x, d, e)) l i
let update_dh  l i x = update (fun (v, f, d, e) -> (v, f, d @ x, e)) l i
let update_eh  l i x = update (fun (v, f, d, e) -> (v, f, d, e @ x)) l i

let delta_emp = ([], [], [], [])

(** retourne la liste des v ou f tel que les variables sont dans le statement ou les hypothèses *)
let get_list proj l eh statement =
  let rec aux l = match l with
  | []   -> []
  | t::q ->
    if (List.mem (proj t) statement)
      || (List.fold_left (fun b (_,_,l) -> b || List.mem (proj t) l) false eh)
    then t::(aux q)
    else aux q
  in
  aux l

(* let context delta i (_, _, statement, _) = (* (lab, t, statement, proof) = *)
  let rec aux l ((v_acc, f_acc, d_acc, e_acc) as acc) i =
    if i = -1 then acc
    else
      match l with
      | []   -> acc
      | (v, f, _, e)::q ->
        let e_curr = e_acc@e in
        let v_curr = get_list (fun v -> v) v e_curr statement in
        let f_curr = get_list (fun (_,_,v) -> v) f e_curr statement in
        aux q (v_acc@v_curr, f_acc@f_curr, d_acc, e_curr) (i-1)
  in
   aux delta delta_emp i *)

let context delta i (_, _, statement, _) = (* (lab, t, statement, proof) = *)
  let rec aux l (v_acc, f_acc, d_acc, e_acc) i =
    if i = -1 then
      get_list (fun v -> v) v_acc e_acc statement,
      get_list (fun (_,_,v) -> v) f_acc e_acc statement,
      [],
      e_acc
    else
      match l with
      | []   ->
        get_list (fun v -> v) v_acc e_acc statement,
        get_list (fun (_,_,v) -> v) f_acc e_acc statement,
        [],
        e_acc
      | (v, f, d, e)::q ->
        aux q (v_acc@v, f_acc@f, d_acc@d, e_acc@e) (i-1)
  in
  aux delta delta_emp i

(* let rec get_ie_first_el l i = match l with
  | [] -> failwith "Internal Error"
  | t::q -> if i = 0 then [t] else t::(get_ie_first_el q (i-1)) *)

let norm file =
  let rec aux_section ((sigma, ax, th) as gamma) delta i section = match section with
    | [] -> gamma, delta
    | BComm(_)::b  -> aux_section gamma delta i b
    | BVdecl(l)::b -> aux_section gamma (update_var delta i l) i b
    | BHypo(Fhypothesis (lab, t, v))::b ->
      aux_section gamma (update_fh delta i [(lab, t, v)]) i b
    | BHypo(Dhypothesis _)::b (* TODO *) -> aux_section gamma delta i b
    | BHypo(Ehypothesis (lab, t, l))::b ->
      aux_section gamma (update_eh delta i [(lab, t, l)]) i b
    | BAssert(Aassertion(l, t, s))::b ->
      aux_section (sigma, ax @ [(context delta i (l, t, s, None), l, t, s)], th) delta i b
    | BAssert(Passertion((l, t, s), p))::b ->
      aux_section (sigma, ax, th @ [(context delta i (l, t, s, Some p), l, t, s, p)]) delta i b
    | BBdecl(sec)::b ->
      let new_gamma, _ = aux_section gamma (delta@[delta_emp]) (i+1) sec in
      aux_section new_gamma delta i b
      (* aux_section new_gamma (get_ie_first_el new_delta i) i b *)
  in
  let rec aux ((sigma, ax, th) as gamma) delta i file = match file with
  | []   -> gamma, delta
  | t::q -> match t with
    | Comm  _ | Impo _ -> aux gamma delta i q
    | Decl (Cdeclaration l) -> aux (sigma@l, ax, th) delta i q
    | Decl (Vdeclaration l) -> aux gamma (update_var delta i l) i q
    | Hypo (Fhypothesis (lab, t, v)) -> aux gamma (update_fh delta i [(lab, t, v)]) i q
    | Hypo (Dhypothesis _) (* TODO *) -> aux gamma delta i q (* aux gamma (update_dh delta i d) i q *)
    | Hypo (Ehypothesis (lab, t, l)) -> aux gamma (update_eh delta i [(lab, t, l)]) i q
    | Asser(Aassertion(l, t, s)) ->
      aux (sigma, ax @ [(context delta i (l, t, s, None), l, t, s)], th) delta i q
    | Asser(Passertion((l, t, s), p)) ->
      aux (sigma, ax, th @ [(context delta i (l, t, s, Some p), l, t, s, p)]) delta i q
    | Decl (Bdeclaration section) ->
      let new_gamma, _ = aux_section gamma (delta@[delta_emp]) (i+1) section in
      aux new_gamma delta i q
      (* aux new_gamma (get_ie_first_el new_delta i) i q *)
  in
  aux ([], [], []) [delta_emp] 0 file



(** Last step of the normalizing processus, using a pre normalized file f *)
(* TODO goal of this step? *)
let normalize_bis_file file = norm file


(* let (c_ty, c_sp, c_op, var, fh), pre_n = norm file in

  if !debug_symbol then
    (let f s = pp _STDOUT " %s " s in
     Printf.printf "TYPES :" ;
     List.iter f c_ty ;
     Printf.printf "\nSPECIAL :" ;
     List.iter f c_sp ;
     Printf.printf "\nOP :" ;
     List.iter f c_op ;
     Printf.printf "\nVAR :" ;
     List.iter (fun (v,_) -> pp _STDOUT " %s " v) var  (* TODO Used only here *)
    ) ;
  (* The normalized file starts with three c-declarations that define every file's constants,
     and a v-declaration that defines its variables *)
   ((c_ty, c_sp, c_op, var, pre_n), fh_map_ordered_to_fh_map_label fh) *)



open MM_common.Error
open MM_common.C_printer_file

let rec pp_axiom = function
  | []   -> ()
  | ((v,f,_,e), l, t, s)::q ->
    pp _STDOUT "${\n" ;
    pp_variable v ;
    List.iter pp_floating f ;
    List.iter pp_essential e ;
    pp_statement (l, t, s) true ; pp _STDOUT "$. " ;
    pp _STDOUT "$}\n" ;
    pp_axiom q

let rec pp_theorem = function
  | []   -> ()
  | ((v,f,_,e), l, t, s, p)::q ->
    pp _STDOUT "${\n" ;
    pp_variable v ;
    List.iter pp_floating f ;
    List.iter pp_essential e ;
    pp_statement (l, t, s) false ; pp_proof p ; pp _STDOUT "$. " ;
    pp _STDOUT "$}\n" ;
    pp_theorem q
