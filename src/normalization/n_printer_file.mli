open N_types
open MM_common.C_types

(** [pp_n_file c_ty, c_pr, c_op, va, pre_n] *)
val pp_n_file : c_decl * c_decl * c_decl * (math_symbol * string) list * n_file -> unit
