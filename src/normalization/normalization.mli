open Parsing.P_types
open N_types
open MM_common.C_types
open MM_common.Data_structure

val debug_symbol : bool ref

val normalize_file :
  file -> (c_decl * c_decl * c_decl *
          (string * string) list * n_file) * (typecode * variable) V_Decl.t
