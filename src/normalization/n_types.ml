open MM_common.C_types

(** Definition of a normalized assertion.
    In Metamath syntax, it could be seen as a block built with
    the assertion and every hypotheses linked to its variables *)
type normalized_assertion =
  f_hypo list * d_hypo list * e_hypo list * statement

type n_assert =
  | SynAxiom of normalized_assertion
  | SemAxiom of normalized_assertion
  | Proof    of normalized_assertion * proof

(** Definition of normalized Metamath commands. *)
type n_command =
  | NImpo  of import
  | NVar   of v_decl
  | NAsser of n_assert

(** Definition of a completely normalized Metamath file,
    that can be used for a translation in Dedukti. *)
type n_file = n_command list
