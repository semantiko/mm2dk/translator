open MM_common.C_types
open MM_common.Error

exception NoLabel

let main_type   = "token"
let main_inj    = "Prf"
let forall      = "∀_MM"
let implication = "⇒_MM"
let concat_sym  = "++"
let glue        = "#"

(** Prelude *)

let pp_prelude ppc =
  Format.fprintf ppc "constant symbol %s : TYPE;\n" main_type ;
  Format.fprintf ppc "injective symbol %s : %s → TYPE;\n\n" main_inj main_type ;
  let type_forall s = "(" ^ s ^ " → " ^ s ^ ") → " ^ s in
  Format.fprintf ppc "symbol %s : %s;\n" forall (type_forall main_type) ;
  Format.fprintf ppc "rule %s (%s (λ a, $b.[a] ) ) ↪ Π a, %s $b.[a];\n" main_inj forall main_inj ;
  let common_typ s = s ^ " → " ^ s ^ " → " ^ s in
  Format.fprintf ppc "symbol %s : %s;\n" implication (common_typ main_type) ;
  Format.fprintf ppc "notation %s infix right 10;\n" implication ;
  Format.fprintf ppc "rule %s ($a %s $b) ↪ %s $a → %s $b;\n\n" main_inj implication main_inj main_inj ;
  Format.fprintf ppc "symbol %s : %s;\n" concat_sym (common_typ main_type) ;
  Format.fprintf ppc "notation %s infix right 10;\n" concat_sym ;
  Format.fprintf ppc "rule ($a %s $l) %s $m ↪ $a %s ($l %s $m);\n\n" concat_sym concat_sym concat_sym concat_sym ;
  Format.fprintf ppc "injective symbol %s : %s;\n" glue (common_typ main_type) ;
  Format.fprintf ppc "notation %s infix right 10;\n\n" glue

(** Symbol *)

let pp_signature ppc name x = Format.fprintf ppc "constant symbol %s : %s;\n" (name x) main_type

(** Axiom *)

let rec token_l ppc name = function | [] -> failwith "No content" | [a] -> Format.fprintf ppc "%s " (name a) | t::q -> Format.fprintf ppc "%s %s " (name t) concat_sym ; token_l ppc name q

let print_statement ppc name v f e l t s =
  Format.fprintf ppc "symbol %s : Prf (" (name l) ;
  let a v = Format.fprintf ppc "%s (λ %s, " forall (name v) in
  List.iter a v ;
  let g (_,t,v) = Format.fprintf ppc "( %s %s %s ) %s " (name t) glue (name v) implication in
  List.iter g f ;
  let h (_,t,s) = Format.fprintf ppc "( %s %s " (name t) glue ; token_l ppc name s ; Format.fprintf ppc ") %s " implication in
  List.iter h e ;
  Format.fprintf ppc "( %s %s " (name t) glue ; token_l ppc name s ; Format.fprintf ppc "))" ; List.iter (fun _ -> Format.fprintf ppc ")") v

let pp_axiom ppc name ax =
  let f ((v,f,_,e), l, t, s) =
    Format.fprintf ppc "constant " ;
    print_statement ppc name v f e l t s ;
    Format.fprintf ppc "%s" ";\n"
  in
  List.iter f ax

(** Theorem *)

let rec get_data ax th label = match ax with
  | [] -> (match th with
      | [] -> raise NoLabel (* failwith ("No label: " ^ label) *)
      | ((v,f,d,e), l, t, s, _)::q -> if label = l then ((v,f,d,e), l, t, s) else get_data ax q label)
  | (((_,_,_,_), l, _, _) as t)::q -> if label = l then t else get_data q th label

let translate_proof name ax th f e p =
  let stack_proof = Stack.create () in
  let rec aux = function
    | [] -> Stack.pop stack_proof
    | (Qmark _)::_ -> failwith "Proof not yet completed"
    | (Label x)::q ->
      if List.fold_left (fun acc (label,_,_) -> acc || x = label) false f
      || List.fold_left (fun acc (label,_,_) -> acc || x = label) false e
      then (Stack.push (name x) stack_proof ; aux q)
      else
        let ((v,f,_,e), _, _, _) = get_data ax th x in
        let tmp  = List.fold_right (fun _ acc -> acc ^ "_ " ) v ((name x) ^ " ") in
        let rec aux2 acc = function
          | [] -> acc
          | _::q ->
            let res = Stack.pop stack_proof in
            if String.contains res ' '
            then aux2 ("( " ^ res ^ ") " ^ acc) q
            else aux2 (res ^ " " ^ acc) q
        in
        (* let tmp2 = List.fold_right (fun acc _ -> acc ^ Stack.pop stack_proof) tmp  in *)
        Stack.push (tmp ^ (aux2 (aux2 "" e) f)) stack_proof ; aux q
  in
  aux p

let pp_th ppc name ax th =
  let rec aux = function
    | []   -> ()
    | ((v,f,_,e), l, t, s, p)::q ->
      print_statement ppc name v f e l t s ;
      try
        let proof = translate_proof name ax th f e p in
        (match v, f, e with
         | [], [], [] -> Format.fprintf ppc "%s" " ≔ " ; Format.fprintf ppc "%s" proof ; Format.fprintf ppc "%s" ";\n"
         | _ ->
           Format.fprintf ppc "%s" " ≔\n  λ" ;
           List.iter (fun x -> Format.fprintf ppc " %s" (name x)) v ; List.iter (fun (x,_,_) -> Format.fprintf ppc " %s" (name x)) f ;
           List.iter (fun (x,_,_) -> Format.fprintf ppc " %s" (name x)) e ; Format.fprintf ppc ", " ;
           Format.fprintf ppc "%s" proof ; Format.fprintf ppc "%s" ";\n") ;
        aux q
      with NoLabel -> wrn_1 _STDOUT "Partial translation - Free variable in %s." l ; Format.fprintf ppc "%s" "; // Fix the proof \n" ; aux q
  in
  aux th
