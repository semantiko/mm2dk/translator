open MM_common.C_types
open MM_common.Data_structure
open MM_common.Error
open Json_running

(** A. To translate some constants into types in Dedukti. *)

(** [from_constant_to_type command_l] translates the list of constants
    [command_l] into a list of types with the Dialekto types. *)
let from_constant_to_type command_l =
  let f c = Constructor.Command.cr_symbol_default Static c ([], Type) in
  List.map f command_l

(** B. To translate some constants into symbols in Dedukti. *)

(** [translate_mixfix s] returns a mixfix has defined in Dialekto that is
    the translation of the given string [s], or raise an error if the
    string doesn't correspond to any mixfix. *)
let translate_mixfix s =
  if s = "prefix"               then Type.Command.Prefix
  else if s = "postfix"         then Type.Command.Postfix
  else if s = "closed"          then Type.Command.Closed
  else if s = "infix left"      then Type.Command.Infix(Left)
  else if s = "infix right"     then Type.Command.Infix(Right)
  else if s = "infix non-assoc" then Type.Command.Infix(Non_assoc)
  else failwith ("UserError - Wrong associativity definition " ^ s ^ " in json file.")

(** [find_type_map fh_data var] returns an option that might contains the
    type of the variable [var] associated to a f-hypothesis in the map
    [fh_data], where (key, value) = (type, variable). *)
let find_type_map fh_data var =
  let f _ (a, b) res =
    if not(b = var)
    then res
    else
      match res with
      | None -> Some(a)
      | _ -> raise (MM2DKError (IllFormedMetamathFile, "axiomatization.ml", "find_type_map", "Variable " ^ b ^ " typed in two or more f-hypotheses."))
  in
  V_Decl.fold f fh_data None

(** [syn_to_sign symb typ statement fh_data add_prop] returns a Dialekto
    command that declare the constant/operator [symb] in Dedukti, with the
    type [typ] and thanks to the statement [statement] of the normalized
    syntaxical axiom and [add_prop] which contains the additionnal
    properties defined in the JSON file. *)
let syn_to_sign symb typ statement fh_data add_prop =
  (* Auxiliary function that use f-hypotheses list's typecodes to
     create the signature of symb, using Dialekto's types. *)
  let f x t = match find_type_map fh_data x with
    | None    -> t
    | Some(k) -> Type.Term.Arrow(Sym(k), t)
  in
  let asser_to_term s = List.fold_right f s (Type.Term.Sym(typ)) in
  let typ = asser_to_term statement in
  if (V_Decl.mem symb add_prop) then
    let prop = V_Decl.find symb add_prop in
    Constructor.Command.cr_symbol ({mixfix = translate_mixfix (prop.mixfix) ; prec = prop.precedence}) Public Static (prop.name_dk) ([], typ)
  else Constructor.Command.cr_symbol_default Static symb ([], typ)

(** [write_sp_ope spe_op_set add_prop] returns a list of objects defined in
    Dialekto that can be used to write declarations of special operators in
    Dedukti.
      - [spe_op_set] is a set of special operators
      - [add_prop]   is a map with
             keys = names of the special operator
             values = its additionnal properties given in the JSON file *)
let write_sp_ope spe_op_set add_prop =
  let rec aux acc = function (* This List.map can delete some values. *)
    | [] -> acc
    | elt::q ->
      try
        let prop = V_Decl.find elt add_prop in
        match prop.type_dk with
        | None -> wrn_1 _STDOUT "%s" ("MissingDataJSONFile - Symbol " ^ elt ^ " untyped in the json file.") ; aux acc q
        | Some(t) ->
          aux ((Constructor.Command.cr_symbol ({mixfix = translate_mixfix (prop.mixfix) ; prec = prop.precedence}) Public Static (prop.name_dk) ([], Sym(t)))::acc) q
      with Not_found -> wrn_1 _STDOUT "%s" ("MissingDataJSONFile - Symbol " ^ elt ^ " cannot be infered. Add its additionnal properties in the JSON file.") ; aux acc q
  in
  aux [] spe_op_set

(** C. To translate the syntactical axioms in Dedukti. *)

(** [check_symb_decl syn_assert fh_data cte_op_data op_trans] returns the statemement of the axiom,
    op_declared updated with the assertion and a flag that indicates if this syntaxical axiom is for subtyping
    requires (syn_assert : the assert defined in a normalized syntaxical axiom)
             (fh_data : the normalized axiom's list of f hypotheses)
             (cte_op_data : a set of the normalized file's basic operators)
             (op_trans : a set of operators already translated in Dedukti) *)
let check_symb_decl syn_assert fh_data cte_op_data op_trans =
  let (la, _, mms) = syn_assert in
  match mms with
  | [ms] -> (* The shape of the axiom is: typecode ms *)
    (match fh_data with
    | [] ->
      if (List.mem ms cte_op_data) && not(List.mem ms op_trans) then (* ms is an operator but not yet translated *)
        (ms, ms::op_trans, false)
      else failwith ("MMFileError - Wrong declaration of a constant operator: " ^ ms)
    | _::_ -> (ms, op_trans, true)) (* Axiom of subtyping *)
  | _ ->
    let rec aux mms res op_trans = match mms with
      | [] -> (
          match res with
          | None -> failwith ("MMFileError - Syntaxical axiom labeled " ^ la ^ " is declared but doesn't infer any operator.")
          | Some(k) -> (k, op_trans, false)
            )
      | ms::t ->
        if not(List.mem ms cte_op_data) (* Si ce n'est pas un opérateur *)
        then aux t res op_trans
        else
          if (List.mem ms op_trans) (* Si déjà traduit *)
          then (wrn_1 stdout ("WARNING: %s") ("MMFileError - Operator " ^ ms ^ " infered in another syntaxical axioms than the one labeled " ^ la) ; aux t (Some(ms)) op_trans)
          else
            if res = None
            then aux t (Some(ms)) (ms::op_trans)
            else failwith ("MMFileError - Two or more operators are infered in the syntaxical axiom labeled " ^ la)
    in
    aux mms None op_trans

(** [syn_to_subtype] : TODO *)
let syn_to_subtype () = raise (MM2DKError (NotYetImplemented, "axiomatization.ml", "syn_to_subtype", "Subtyping not implemented yet."))

(** D. To translate the semantical  axioms in Dedukti. *)

(** [sem_axiom_to_string eh_data asser cte_op_data op_trans add_prop flag]
    returns a list of strings that are parts of the axiom/proof statement, and the label of the assertion.
    requires (eh_data : a list of local hypotheses of a normalized semantical axiom or a normalized proof)
    (asser : the assertion given in the axiom/proof)
   (cte_op_data : the set of every basic operators of the normalized file)
   op_trans : a set of operators already translated in Dedukti) ;   *)
let sem_axiom_to_string (*dh_data *) eh_data asser cte_op_data op_trans add_prop flag =
  (* mms_to_string : requires (mms : a sequence of math-symbols) (op_set : the set of every basic operators of the normalized file)
     (op_declared : a set of operators already translated in Dedukti) ;
     returns mms converted into a string only if every operator contained in mms are already translated in Dedukti. *)
  let rec aux mms res = match mms with
    | [] -> res
    | ms::t ->
      if not(List.mem ms cte_op_data) (*ms is not an operator (it could be a variable for instance) *) || ((List.mem ms cte_op_data) && (List.mem ms op_trans))
      then aux t (res ^ " " ^ ms)
      else raise (MM2DKError (IllFormedMetamathFile, "axiomatization.ml", "mms_to_string", "Operator " ^ ms ^ " used in a semantical axiom's local hypothesis but undeclared previously."))
  in
  let mms_to_string mms = aux mms "" in
  (* Auxiliary function that takes a list of e-hypotheses and converts it into a list of hypotheses translated into strings. *)
  let str mms = rename_op_with_JSON_data (mms_to_string mms) add_prop in
  let f (_, ty, mms) = ty ^ " " ^ (str mms) in
  let str_statement = match asser with
    | Aassertion((label, ty, mms)) | Passertion((label, ty, mms), _) ->
      if flag then ty (* si f-hypo *) else f (label, ty, mms) (* sinon *)
  in
  (List.map f eh_data, str_statement)

(** [translate_type fh_data eh_data str_statement add_prop] builds a Dedukti type
    thanks to the f-hypotheses [fh_data] and the e-hypotheses [eh_data].
    The general shape is: \Pi f1 ... fx, e1 -> ... -> en -> [str_statement]. *)
let translate_type fh_data eh_data str_statement add_prop = (* TODO remove add_prop *)
  let f x t = Type.Term.Arrow(Sym(rename_op_with_JSON_data x add_prop), t) in
  let build_arrow l = List.fold_right f l (Type.Term.Sym(rename_op_with_JSON_data str_statement add_prop)) in
  let g (_, ty, var) t = Type.Term.Pi(([var], Some(Type.Term.Sym(ty))), t) in
  let build_pi l el = List.fold_right g l el in
  build_pi fh_data (build_arrow eh_data)

let translate_sem_axiom label fh_data eh_data str_s add_prop =
  let typ = translate_type fh_data eh_data str_s add_prop in
  Constructor.Command.cr_symbol_default Static label ([], typ)

let translate_proof label fh_data eh_data str_s lambda_t add_prop =
  let typ = translate_type fh_data eh_data str_s add_prop in
  Constructor.Command.cr_definition ({mixfix = Prefix; prec = 42}) Public (Definable(Free)) label ([], typ) (LambdaTerm(lambda_t)) true
