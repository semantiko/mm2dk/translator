open MM_common.C_types
open MM_common.Data_structure
open Norm.N_types
open Axiomatization
open Lambda_term

(** [translation cte_op_data n_f fh_data add_prop] prints the translated Dedukti file in stdout,
       where [cte_op_data] contains all the constant operators,
             [n_f] is a normalized file,
             [fh_data] is a map with key = the label of a f-hypothesis
                               and value = a pair composed of a variable and its type,
             [add_prop] constains the JSON data. *)
let translation cte_op_data n_f fh_data add_prop =
  (* [f_i]
     [op_trans]
     [asser_map] = label |-> assertion *)
  let rec aux f_i op_trans asser_map = match f_i with
    | [] -> []
    | NVar(_)::t | NImpo(_)::t -> aux t op_trans asser_map (* Var suppr WARNING *)

    | NAsser(x)::t ->
      ( match x with
        | SynAxiom(fhypo, _, _, ((label, typ, statement) as asser)) ->
          let (symb, new_op_trans, is_subtyp) = check_symb_decl asser fhypo cte_op_data op_trans in
          let res = if is_subtyp then syn_to_subtype () else syn_to_sign symb typ statement fh_data add_prop in
          let new_asser_map = V_Decl.add label x asser_map in
          res::(aux t new_op_trans new_asser_map)

        | SemAxiom(fhypo,_,ehypo,((label,_,_) as asser)) ->
          let (str_eh, str_s) = sem_axiom_to_string ehypo (Aassertion(asser)) cte_op_data op_trans add_prop false in
          let new_asser_map = V_Decl.add label x asser_map in
          (translate_sem_axiom label fhypo str_eh str_s add_prop)::(aux t op_trans new_asser_map)

        | Proof((fhypo,_,ehypo,((label,_,_) as asser)), proof) ->
          let (l_expr, flag) = build_lambda_expr asser proof fh_data asser_map add_prop in
          let lambda_term = build_lambda fhypo ehypo l_expr in
          let (str_eh, str_s) = sem_axiom_to_string ehypo (Passertion(asser, proof)) cte_op_data op_trans add_prop flag in
          let new_asser_map = V_Decl.add label x asser_map in
          (translate_proof label fhypo str_eh str_s lambda_term add_prop)::(aux t op_trans new_asser_map) )
  in
  aux n_f [] V_Decl.empty
