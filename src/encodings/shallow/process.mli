open MM_common.C_types
open MM_common.Data_structure
open Norm.N_types

open Type.Command

open Json_running

val translation : c_decl -> n_file -> (typecode * variable) V_Decl.t -> json_info V_Decl.t -> command list
