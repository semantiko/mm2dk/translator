open MM_common.Data_structure

(** Informations described in the JSON file that have to be described
    in Dedukti *)
type json_info =
  { name_dk    : string
    (** Name to use in Dedukti *)
  ; type_dk    : string option
    (** Optional type for the symbol named [name_dk] *)
  ; mixfix     : string
    (** Mixfix = "prefix" or "postfix" or "closed" or
                 "infix left" or "infix right" or "infix non-assoc" *)
  ; precedence : int
    (** The precedence - Useful only if the symbol is infix *) }

(** [get_JSON_data p] returns, from the Metamath file's path without
    extension [p], a map where:
     - each key = operator's name in Metamath,
     - each value = a value of type json_info. *)
val get_JSON_data : string -> json_info V_Decl.t

(** [rename_op_with_JSON_data s add_prop] renames each operator in [s]
    thanks to data in [add_prop]. *) (* TODO Elliot - check doc *)
val rename_op_with_JSON_data : string -> json_info V_Decl.t -> string
