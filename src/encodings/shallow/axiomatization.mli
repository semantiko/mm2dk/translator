open MM_common.C_types
open MM_common.Data_structure
open Json_running

val from_constant_to_type : c_decl -> Type.Command.command list

val write_sp_ope : c_decl -> json_info V_Decl.t -> Type.Command.command list

val check_symb_decl :
  string * 'a * multi_math_symbols -> 'b list -> c_decl -> c_decl ->
  string * c_decl * bool

val syn_to_sign :
  string -> string -> multi_math_symbols ->
  (string * string) V_Decl.t -> json_info V_Decl.t ->
  Type.Command.command

val syn_to_subtype : unit -> 'a

val sem_axiom_to_string :
  e_hypo list -> assertion -> c_decl -> c_decl -> json_info V_Decl.t ->
  bool -> string list * string

val translate_sem_axiom :
  string -> f_hypo list -> string list -> string ->
  Json_running.json_info V_Decl.t -> Type.Command.command

val translate_proof :
  string -> f_hypo list -> string list -> string -> Type.Term.term ->
  Json_running.json_info V_Decl.t -> Type.Command.command
