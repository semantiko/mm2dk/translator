open MM_common.Data_structure
open MM_common.Error
open Yojson.Basic
open Yojson.Basic.Util

(** [check_json p] returns a yojson object corresponding to the JSON file
    [p].json, where [p] is the Metamath file's path without extension.
    If the JSON file doesn't exist, a warning is raised, and an empty
    yojson object is returned. *)
let check_json (p : string) : Yojson.Basic.t =
  let json_p = p ^ ".json" in
  if Sys.file_exists json_p
  then from_file json_p
  else
    (wrn_1 _STDOUT "Warning: JSON file %s doesn't exist in repertory. \
                    Some operations might fail." json_p ; from_string "{}")

(** Informations described in the JSON file that have to be described
    in Dedukti *)
type json_info =
  { name_dk    : string
    (** Name to use in Dedukti *)
  ; type_dk    : string option
    (** Optional type for the symbol named [name_dk] *)
  ; mixfix     : string
    (** Mixfix = "prefix" or "postfix" or "closed" or
                 "infix left" or "infix right" or "infix non-assoc" *)
  ; precedence : int
    (** The precedence - Useful only if the symbol is infix *) }

(** Auxiliary functions to get information from the JSON file *)
let get_Metamath_name  oj = oj |> member "name_mm" |> to_string
let get_Dedukti_name   oj =
  try       oj |> member "name_dk" |> to_string
  with _ -> oj |> member "name_mm" |> to_string
let get_type           oj =
  try Some(oj |> member "type"    |> to_string)
  with _ -> None
let get_mixfix         oj = oj |> member "mixfix"  |> to_string
let get_precedence     oj = oj |> member "prec"    |> to_int

(** [get_additionnal_prop oj] returns, from the yojson object [oj],
    a map where:
     - each key = operator's name in Metamath,
     - each value = a value of type json_info. *)
let get_additionnal_prop (oj : Yojson.Basic.t) : json_info V_Decl.t =
  (* STEP 1: Translate the yojson object, i.e. the file, into a list. *)
  let oj_l = try  oj |> to_list with Type_error(_,_) -> [] in (* TODO Elliot - why type_error? *)
  (* STEP 2: Create the map from the file. *)
  let f res oj =
    let key = get_Metamath_name oj in
    let values =
      { name_dk    = get_Dedukti_name oj
      ; type_dk    = get_type oj
      ; mixfix     = get_mixfix oj
      ; precedence = get_precedence oj }
    in
    V_Decl.add key values res
  in
  List.fold_left f V_Decl.empty oj_l

(** [get_JSON_data p] returns, from the Metamath file's path without
    extension [p], a map where:
     - each key = operator's name in Metamath,
     - each value = a value of type json_info. *)
let get_JSON_data (p : string) : json_info V_Decl.t =
  get_additionnal_prop (check_json p)

(** [rename_op_with_JSON_data s add_prop] renames each operator in [s]
    thanks to data in [add_prop]. *) (* TODO Elliot - check doc *)
let rename_op_with_JSON_data (s : string) (add_prop : json_info V_Decl.t) : string =
  let l = Str.split_delim (Str.regexp " ") s in
  let f elt =
    if V_Decl.mem elt add_prop then
      let v = V_Decl.find elt add_prop in
      v.name_dk
    else elt
  in
  let inter = List.map f l in
  let rec aux l res = match l with  
    | []   -> res
    | [x]  -> res ^ x
    | h::t -> aux t (res ^ h ^ " ")
  in
  aux inter ""
