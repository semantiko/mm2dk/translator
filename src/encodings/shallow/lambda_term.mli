open MM_common.C_types
open MM_common.Data_structure
open Norm.N_types
open Json_running

val debug_substitution : bool ref

val build_lambda_expr : statement -> proof -> (typecode * variable) V_Decl.t -> n_assert V_Decl.t -> json_info V_Decl.t -> (string * bool)

val build_lambda : f_hypo list -> e_hypo list -> string -> Type.Term.term
