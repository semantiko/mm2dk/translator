open MM_common.C_types
open MM_common.Data_structure
open MM_common.Error
open Norm.N_types
open Json_running

let debug_substitution = ref false

module Pos = Set.Make(Int)

(** stack_out : requires (fl : a f_hypotheses list from a normalized axiom or proof) (el : a e_hypotheses list from a normalized semantical axiom or proof) 
    (proof_la : the label of a normalized axiom or proof)(fh_m : a map with key = the label of a f_hypothesis and value = a pair composed of a variable and its type, 
    as defined in the hypothesis) (s : a stack of string) ; returns a pair of two lists : the first one containing labels of e_hypothesis or intermediary result built 
    with semantical axioms/other prooves, the second one containing a list of pairs with first element = the new typed expression and second element = the typed 
    expression to be substitued.*)
let stack_out fl el proof_la fh_m s = 
  (*Auxiliary function that unstack form the stack s as much elements as in the list given in parameters *)
  let rec unstack l res = match l with (* = map + rev *)
    | [] -> res
    | _::t -> let k = Stack.pop s in unstack t (k::res)
  in
  (*Second auxiliary function that returns a list of pairs with first element = an expression from la_l and second element = a variable from fl that will be
    substitued with the previous expression *)

  let rec check_typ la_l (fl : f_hypo list) = match la_l, fl with
    |[], [] -> []
    |[], _ |_, [] -> wrn_1 _STDOUT "%s." ("MMFileError - Missing f-hypothesis or label in proof labeled " ^ proof_la) ; [] (* TODO fix? *)
    |la::t1, (_, ty_to_sub, va_to_sub)::t2 ->
      try
        let (new_ty, new_va) = V_Decl.find la fh_m in
        if new_ty = ty_to_sub then (new_va,va_to_sub)::check_typ t1 t2
        else raise (MM2DKError (UnexpectedInternalError, "lambda_term.ml", "check_typ", "Incompatible variable during substitution in the proof " ^ proof_la ^ "."))
      with Not_found -> raise (MM2DKError (UnexpectedInternalError, "lambda_term.ml", "check_typ", "F-hypothesis labeled " ^ la ^ " default in proof " ^ proof_la))
  in
  let e_la_l = unstack el [] in (*List of intermediary expression (local hypothesis or new expression built with other axioms or prooves) built following
                                          the same process of Metamath variable substitution process *)
  let f_la_l = unstack fl [] in    
  let f_sub_l = check_typ f_la_l fl in (*List of pairs with variables and expressions that will replace them*)
  (e_la_l, f_sub_l)

(**substitution : requires (mms : a sequence of math-symbol with variables that have to be substitued) (f_sub_l : a list of pairs with variables and expressions that
   will substitute them) ; returns a string corresponding to mms with substitued variable, using f_sub_l.*)
let substitution mms f_sub_l =
  if !debug_substitution then
    (let f () (a, b) = pp _STDOUT "%s substitued with %s\n" b a in
     List.fold_left f () f_sub_l) ;

  let substitued = ref (Pos.empty) in
  let rec aux2 mms h substi_set i = match mms, h with
    | [], (_,_) -> substitued := substi_set ; []
    | ms::t, (new_va, va_to_sub) ->
       if ms = va_to_sub && not(Pos.mem i substi_set)
       then new_va::(aux2 t h (Pos.add i substi_set) (i+1))
       else ms::(aux2 t h substi_set (i+1))
  in
  let rec aux mms f_sub_l substi_set = match f_sub_l with
    | [] -> mms
    | h::t -> let res = aux2 mms h substi_set 0 in aux res t (!substitued)
  in
  let rec to_string mms res = match mms with  
    | [] -> res
    | ms::t -> to_string t (res ^ " " ^ ms)
  in
  to_string (aux mms f_sub_l (!substitued)) ""

let rec aux2 l f res = match l with
  | []   -> res
  | h::t ->
     let e = f h in
     if String.contains e ' ' then aux2 t f (res ^ " (" ^ e ^ ")") else aux2 t f (res ^ " " ^ e)

(* let rec aux2 l res = match l with
  | []   -> res
   | h::t -> if String.contains h ' ' then aux2 t (res ^ " (" ^ h ^ ")") else aux2 t (res ^ " " ^ h) *)


(* let aux2 l f acc =
  let f x t = if String.contains x ' ' then t ^ " (" ^ x ^ ")" else t ^ " " ^ x in
  List.fold_right f acc l *)


(**write_new_expr : requires (e_la : a list of intermediary expression/e_hypotheses that are used to write an axiom or a proof) (f_sub_l : a list of variables and
   their substitution that are used as required f_hypotheses to write a proof or an axiom) (na_la : the proof/axiom's label in question) ; returns a string corresponding to
   a new expression built with axiom/proof labeled na_la and its hypotheses in the lists in parameters.  *)
let write_new_expr e_la_l f_sub_l na_la = aux2 e_la_l (fun x -> x) (aux2 f_sub_l fst na_la)

(**build_new_term

  requires
     (proof_la : the label of the proof we want to translate)
     (na : the normalized axiom/proof got in the label sequence of the proof labeled
   proof_la, druring its translation process)
     (s : the stack containing label/expression used in the translation process)
     (fh_m : a map with
         key = the label of a f_hypothesis and
         value = a pair composed of a variable and its type, as defined in the hypothesis)
     (i : an counter) ;

   returns a pair containing a new expression/label to push in the stack, and fh_m updated.*)
let build_new_term proof_la na s fh_m i = match na with 
  | SynAxiom(fl, _, el, (_, ty, mms)) ->
     let (_, f_sub_l) = stack_out fl el proof_la fh_m s in
     let new_expr = substitution mms f_sub_l in (*Construction of a new typed expression using variables substitution *)
     let new_label = "#"^(string_of_int i) in (*We give a unique label to this new expression *)
     let new_fh_m = V_Decl.add new_label (ty, new_expr) fh_m in
     (new_label, new_fh_m)
  | SemAxiom(fl, _, el, (na_la, _, _)) | Proof((fl, _, el, (na_la, _, _)), _) ->
     let (e_la_l, f_sub_l) = stack_out fl el proof_la fh_m s in
     let new_hyp = write_new_expr e_la_l f_sub_l na_la in (*New expression creation using an semantical axiom or a proof *)
     (new_hyp, fh_m)

(** build_lambda_expr : requires
     (passert : the assertion we want to translate)
     (proof : the proof linked with the assertion)
     (fh_m : a map with
          key = the label of a f_hypothesis and
          value = a pair composed of a variable and its type, as defined in the hypothesis)
     (asser_m : a map with key = label and value = the assertion linked to this label) ;
 returns a lambda expression built from the sequel of label in passert. *)
let build_lambda_expr passert proof fh_m asser_m add_prop =
  let (p_la,_,_) = passert in
  let s = Stack.create () in
  let i = 0 in
  let rec aux pae fh_m i = match pae with
    | Qmark(_)::_ -> raise (MM2DKError (IllFormedMetamathFile, "lambda_term.ml", "build_lambda_expr", p_la ^ " is an uncompleted proof."))
    | [] -> (* We need to check that the last element in the stack is syntactically equal to the statement. *)
      let k = Stack.pop s in
      if (V_Decl.mem k fh_m) then
        let (_, va) = V_Decl.find k fh_m in
        (rename_op_with_JSON_data va add_prop, true)
      else (rename_op_with_JSON_data k add_prop, false)
    | Label(la)::t ->
      if not(V_Decl.mem la asser_m) then (* If the label isn't associated to an axiom or a proof. = $f or $e *)
        (Stack.push la s ; aux t fh_m i)
      else (* If the label is associated to an axiom or a proof. = $a or $p *)
        let na = V_Decl.find la asser_m in
        let (new_expr, new_fh_m) = build_new_term p_la na s fh_m i in
        Stack.push new_expr s ; aux t new_fh_m (i+1)
  in
  aux proof fh_m i

let aux (l : ('a * 'b * 'c) list) (f : ('a * 'b * 'c) -> string) res : Type.Term.term =
  List.fold_right (fun el t -> Type.Term.Lambda(([f el], None), t)) l res

let first (v : 'a * 'b * 'c) : 'a = let (x, _, _) = v in x
let third (v : 'a * 'b * 'c) : 'c = let (_, _, x) = v in x

(**build_lambda : requires (fhypo_l : list of f_hypotheses required to build the lambda term) (ehypo_l : " " e_hypotheses " " " " " ") (lambda_expr : the lambda term
   expression built usingthe previous function) ; returns a Dedukti command that will be used to translate a Metamath proof into a lambda term in Dedukti. *)
let build_lambda (fhypo_l : f_hypo list) (ehypo_l : e_hypo list) lambda_expr =
  let e_hypo_res = aux ehypo_l first (Type.Term.Sym(lambda_expr)) in
  aux fhypo_l third e_hypo_res
