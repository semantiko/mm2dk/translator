open MM_common.C_types

(** Definition of the Metamath commands *)
type command =
  | Comm  of comment
  | Impo  of import
  | Decl  of declaration
  | Hypo  of hypothesis
  | Asser of assertion


(** Definition of a Metamath file *)
type file = command list
