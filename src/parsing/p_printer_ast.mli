open P_types

(** Function to print the parsed file as AST in Stdout *)
val print_ast : file -> unit
