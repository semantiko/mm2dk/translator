%{
  open MM_common.C_types
%}

%token <MM_common.C_types.text> TEXT
%token <MM_common.C_types.filename> FILENAME
%token <MM_common.C_types.math_symbol> MATH_SYMBOL

%token DOLL_BRACK_LEFT DOLL_BRACK_RIGHT /* $[ $] - file include */

%token DOLL_C                                  /* $c */
%token DOLL_V                                  /* $v */

%token DOLL_D                                  /* $d */
%token <MM_common.C_types.label> LABEL_DOLL_F  /* $f */
%token <MM_common.C_types.label> LABEL_DOLL_E  /* $e */

%token <MM_common.C_types.label> LABEL_DOLL_A  /* $a */
%token <MM_common.C_types.label> LABEL_DOLL_P  /* $p */
%token <MM_common.C_types.proof> DOLL_EQ       /* $= */
%token DOLL_DOT                                /* $. */

%token DOLL_BRACE_LEFT                         /* ${ */
%token DOLL_BRACE_RIGHT                        /* $} */

/* ? symbols that can be found after a $= in a p-assert*/
/* %token QMARK */

%token EOF /* End of file token */

%start file

%type <MM_common.C_types.multi_math_symbols> math_symbols

%type <MM_common.C_types.f_hypo> f_hypo
%type <MM_common.C_types.e_hypo> e_hypo
%type <P_types.command> command
%type <P_types.file> file

%%

math_symbols:
  | MATH_SYMBOL* { $1 }

comment:
  | TEXT { $1 }

import:
  | DOLL_BRACK_LEFT FILENAME* DOLL_BRACK_RIGHT { match $2 with
                                                 | [] -> failwith "At least one importation should be declared in import declaration."
                                                 | t::q -> t::q }

c_decl:
  | DOLL_C MATH_SYMBOL* DOLL_DOT { match $2 with
                                   | [] -> failwith "At least one math symbol should be declared in constant declaration."
                                   | t::q -> t::q }

v_decl:
  | DOLL_V MATH_SYMBOL* DOLL_DOT { match $2 with
                                   | [] -> failwith "At least one math symbol should be declared in variable declaration."
                                   | t::q -> t::q }

d_hypo:
  | DOLL_D d_hypo_var DOLL_DOT { $2 }

d_hypo_var:
  | MATH_SYMBOL MATH_SYMBOL { DhypoOne($1, $2)  }
  | d_hypo_var  MATH_SYMBOL { DhypoMany($1, $2) }

f_hypo:
  | LABEL_DOLL_F MATH_SYMBOL MATH_SYMBOL DOLL_DOT  { ($1, $2, $3) }

e_hypo:
  | LABEL_DOLL_E MATH_SYMBOL math_symbols DOLL_DOT { ($1, $2, $3) }

a_assert:
  | LABEL_DOLL_A MATH_SYMBOL math_symbols DOLL_DOT { Aassertion(($1, $2, $3)) }

p_assert:
  | LABEL_DOLL_P MATH_SYMBOL math_symbols DOLL_EQ  { Passertion(($1, $2, $3), $4) }

hypothesis:
  | d_hypo { Dhypothesis($1) }
  | f_hypo { Fhypothesis($1) }
  | e_hypo { Ehypothesis($1) }

assertion:
  | a_assert { $1 }
  | p_assert { $1 }

b_decl:
  | DOLL_BRACE_LEFT in_block* DOLL_BRACE_RIGHT { $2 }

in_block:
  | comment    { BComm($1)   }
  | v_decl     { BVdecl($1)  }
  | hypothesis { BHypo($1)   }
  | assertion  { BAssert($1) }
  | b_decl     { BBdecl($1)  }

declaration:
  | c_decl { Cdeclaration($1) }
  | v_decl { Vdeclaration($1) }
  | b_decl { Bdeclaration($1) }

command:
  | comment     { Comm  $1 }
  | import      { Impo  $1 }
  | hypothesis  { Hypo  $1 }
  | assertion   { Asser $1 }
  | declaration { Decl  $1 }

file:
  | command* EOF { $1 }
