open P_types
open MM_common.C_printer_ast

let pp_command (command : command) =
  let tab = ["|\t"] in
  print_sep tab "\n" ;
  match command with
  | Comm c -> pp_aux_comm tab c
  | Impo i ->
    print_sep_spec tab  ; pp "Import\n" ;
    print_sep tab "|\n" ; pp_aux_import (["|\t"; "|\t"]) i
  | Decl  d -> pp_aux_decl   tab d
  | Asser s -> pp_aux_assert tab s
  | Hypo  h -> pp_aux_hypo   tab h

(** Function to print the parsed file as AST in Stdout *)
let print_ast f = pp "File\n" ; List.iter pp_command f ; pp "EoF\n"
