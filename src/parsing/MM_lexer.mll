{
  open MM_parser

  exception UnexpectedToken of int * int
  exception UnexpectedEOF
  exception UnexpectedProofContent
}

let carac = ['a'-'z''A'-'Z''0'-'9']
let special_carac = ['!' '"' '#' '%' '&' ''' '(' ')' '*' '+' ',' '-' '.' '/' ':' ';' '<' '=' '>' '?' '@' '[' ']' '\\' '^' '_' '`' '{' '|' '}' '~']
let white_space = [' ' '\t' '\r' '\n'] (* \f missing *)

let text        = (carac | special_carac | white_space | '$')+
let filename    = (carac | special_carac)+".mm"
let math_symbol = (carac | special_carac)+
let typecode    = (carac | special_carac)+
let variable    = (carac | special_carac)+
let label       = (carac | ['-' '_' '.'])+
let newline     = '\n' | '\r''\n'

rule token = parse
  | [' ' '\t'] | newline                         { token   lexbuf           }
  | "$("                                         { comment lexbuf           }
  | "$["                                         { DOLL_BRACK_LEFT          }
  | "$]"                                         { DOLL_BRACK_RIGHT         }
  | "${"                                         { DOLL_BRACE_LEFT          }
  | "$}"                                         { DOLL_BRACE_RIGHT         }
  | "$c"                                         { DOLL_C                   }
  | "$v"                                         { DOLL_V                   }
  | "$d"                                         { DOLL_D                   }
  | "$="                                         { DOLL_EQ (doll_eq lexbuf) }
  | "$."                                         { DOLL_DOT                 }
  | (label as l)((newline | [' ' '\t'])*)" $f"   { LABEL_DOLL_F l           }
  | (label as l)((newline | [' ' '\t'])*)" $e"   { LABEL_DOLL_E l           }
  | (label as l)((newline | [' ' '\t'])*)" $a"   { LABEL_DOLL_A l           }
  | (label as l)((newline | [' ' '\t'])*)" $p"   { LABEL_DOLL_P l           }
  | math_symbol as ms                            { MATH_SYMBOL ms           }
  | filename as fn                               { FILENAME fn              } (* TODO fix me *)
  | eof                                          { EOF                      }
  | _                                            {
                                                   let curr = lexbuf.Lexing.lex_curr_p in
                                                   let line = curr.Lexing.pos_lnum in
                                                   let cnum = curr.Lexing.pos_cnum - curr.Lexing.pos_bol in
                                                   raise (UnexpectedToken (line, cnum))
                                                 }

and comment = parse
  | "$)"                                         { token   lexbuf      }
  | _                                            { comment lexbuf      }
  | eof                                          { raise UnexpectedEOF }

and doll_eq = parse
  | "$."                                         { []                            }
  | [' ' '\t'] | newline                         { doll_eq lexbuf                }
  | label as l                                   {   (Label l)::(doll_eq lexbuf) }
  | '?'                                          { (Qmark '?')::(doll_eq lexbuf) }
  | "$("                                         { comment_doll_eq lexbuf        }
  | _                                            { raise UnexpectedProofContent  }

and comment_doll_eq = parse
  | "$)"                                         { doll_eq lexbuf         }
  | _                                            { comment_doll_eq lexbuf }
  | eof                                          { raise UnexpectedEOF    }
