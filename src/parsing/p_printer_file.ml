open MM_common.C_printer_file
open P_types

let pp_command = function
  | Comm  _ -> ()
  | Impo  i -> pp_import i
  | Decl  d -> pp_declaration d
  | Hypo  h -> pp_hypothesis h
  | Asser a -> pp_assertion a

let pp_file file = List.iter pp_command file
