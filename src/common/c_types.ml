(** Definition of basic terminals and non-terminals.
    WARNING: The terminals "$f", "$e", "$a" and "$p" are managed
             by the lexer. *)

type 'a ne_list =
  | NE_nil  of 'a
  | NE_cons of 'a * 'a ne_list

(** Some atomic types *)
type text = string

type filename = string

type math_symbol = string

type multi_math_symbols = math_symbol list

type variable = math_symbol

type typecode = math_symbol

type label = string

(** Definition of comment. *)
type comment = text

(** Definition of source file import. *)
type import = filename list (* ne_list serait mieux *)

(** Definition of constant declaration. *)
type c_decl = math_symbol list (* ne_list serait mieux *)

(** Definition of variable declaration. *)
type v_decl = math_symbol list (* ne_list serait mieux *)

(** Definition of hypothesis. *)

(* Disjoint-variable restriction *)
type d_hypo =
  | DhypoOne  of variable * variable
  | DhypoMany of d_hypo   * variable

(* Variable-type hypotheses *)
type f_hypo = label * typecode * variable

(* Logical hypotheses *)
type e_hypo = label * typecode * multi_math_symbols

type hypothesis =
  | Dhypothesis of d_hypo
  | Fhypothesis of f_hypo
  | Ehypothesis of e_hypo

(** Definition of assertions (axiomatic and provable) *)

type statement = label * typecode * multi_math_symbols

type el_proof = Qmark of char | Label of label
type proof = el_proof list

type assertion = Aassertion of statement | Passertion of statement * proof

(** Definition of block declaration. *)
type in_block =
  | BComm   of comment
  | BVdecl  of v_decl
  | BBdecl  of b_decl
  | BHypo   of hypothesis
  | BAssert of assertion
and b_decl = in_block list

(** Definition of concrete declaration. *)
type declaration =
  | Cdeclaration of c_decl
  | Vdeclaration of v_decl
  | Bdeclaration of b_decl
