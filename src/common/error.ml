
let no_color = ref false

(** Format transformers (colors). *)
let colorize k fmt =
  if !no_color then fmt else "\027[" ^^ k ^^ "m" ^^ fmt ^^ "\027[0m%!"

(** Some colors *)
let gre fmt = colorize "32" fmt
let yel fmt = colorize "33" fmt
let red fmt = colorize "31" fmt

(** Parameter for printing *)

let pp      = Printf.fprintf (* ppc = pretty printing channel *)
let _STDOUT = stdout

(* There is no dependent type in OCaml, so I need to duplicate the
   following function, one for each number of parameters. *)
let msg   ppc = fun msg -> pp ppc msg ; pp ppc "\n"
let msg_1 ppc = fun msg arg -> pp ppc msg arg ; pp ppc "\n"
let msg_2 ppc = fun msg arg1 arg2 -> pp ppc msg arg1 arg2 ; pp ppc "\n"

(** Coloried message *)

let wrn_msg ppc = fun msg -> pp ppc (yel msg) ; pp ppc "\n"
let wrn_1 ppc = fun msg arg -> pp ppc (yel msg) arg ; pp ppc "\n"
let wrn_2 ppc = fun msg arg1 arg2 -> pp ppc (yel msg) arg1 arg2 ; pp ppc "\n"
let wrn_3 ppc = fun msg arg1 arg2 arg3 ->
  pp ppc (yel msg) arg1 arg2 arg3 ; pp ppc "\n"

let err_msg ppc = fun msg -> pp ppc (red msg) ; pp ppc "\n"
let err_1 ppc = fun msg arg -> pp ppc (red msg) arg ; pp ppc "\n"
let err_2 ppc = fun msg arg1 arg2 -> pp ppc (red msg) arg1 arg2 ; pp ppc "\n"
let err_3 ppc = fun msg arg1 arg2 arg3 ->
  pp ppc (red msg) arg1 arg2 arg3 ; pp ppc "\n"

type mm2dkTypeError  = UnexpectedInternalError | UserError | NotYetImplemented | IllFormedMetamathFile

(** A MM2DK error has the following information:
      type error * file name * function name * specific message to specify the error *)
exception MM2DKError of mm2dkTypeError * string * string * string
