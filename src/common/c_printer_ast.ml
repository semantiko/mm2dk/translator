open C_types

let pp = Printf.printf
(* let pp_tab = pp "|\t" TODO - used it *)

let print_sep tab at_the_end = List.iter print_string tab ; pp "%s" at_the_end

let rec print_sep_spec = function
  | []     -> ()
  | [_]    -> print_string "|-"
  | x :: q -> print_string x ; print_sep_spec q

let pp_ne_list tab s l =
  let rec aux l =
    print_sep_spec tab ; match l with
    | [] -> failwith "The non-empty list is empty!"
    | [x]  -> pp "%s(%s)\n" s x
    | x::q -> pp "%s(%s)\n" s x ; print_sep tab "\n" ; aux q
  in
  aux l

(** Auxiliary functions that are used by the functions [print_file], to
    show the parsed file, and [print_normalized_file] to show the normalized
    file *)

let pp_aux_comm tab _ = print_sep_spec tab ; pp "Comm\n"

let pp_aux_import tab a = pp_ne_list tab "Imp" a

let pp_aux_math_symbols tab (a : multi_math_symbols) =
  let f s = print_sep_spec tab ; pp "Symb(%s)\n" s ; print_sep tab "\n" in
  List.iter f a

let rec pp_aux_dhypo tab a =
  print_sep_spec tab ;
  match a with
  | DhypoOne(v1, v2) ->
     pp "Var(%s)\n" v1 ; print_sep tab "\n" ; print_sep_spec tab ; pp "Var(%s)\n" v2
  | DhypoMany(h, v)  ->
     pp "Var(%s)\n" v  ; print_sep tab "\n" ; pp_aux_dhypo tab h

let pp_aux_fhypo tab (a : f_hypo) =
  let (la, t, v) = a in
  print_sep_spec tab ; pp "Lab(%s)\n" la ; print_sep tab "\n" ;
  print_sep_spec tab ; pp "Typ(%s)\n" t  ; print_sep tab "\n" ;
  print_sep_spec tab ; pp "Var(%s)\n" v

let pp_aux_ehypo tab (a : e_hypo) =
  let (la, t, m) = a in
  print_sep_spec tab ; pp "Lab(%s)\n" la ; print_sep tab "\n" ;
  print_sep_spec tab ; pp "Typ(%s)\n" t  ; print_sep tab "\n" ;
  pp_aux_math_symbols tab m

let pp_aux_aassert tab (a : statement) = pp_aux_ehypo tab a

let rec pp_aux_passert_end tab (a : proof) = match a with
  | [] -> ()
  | Qmark(_)::p  -> print_sep_spec tab ; pp "?\n"          ; print_sep tab "\n" ;
     pp_aux_passert_end tab p
  | Label(la)::p -> print_sep_spec tab ; pp "Lab(%s)\n" la ; print_sep tab "\n" ;
     pp_aux_passert_end tab p

let pp_aux_passert tab (a : statement) p =
  let (la,t,m) = a in
  print_sep_spec tab ; pp "Lab(%s)\n" la ; print_sep tab "\n" ;
  print_sep_spec tab ; pp "Typ(%s)\n" t  ; print_sep tab "\n" ;
  pp_aux_math_symbols tab m ;
  print_sep_spec tab ; pp "$=\n" ; print_sep tab "|\n" ;
  pp_aux_passert_end (tab@["|\t"]) p

let pp_aux_cdecl tab a = pp_ne_list tab "Const" a

let pp_aux_vdecl tab a = pp_ne_list tab "Var"   a

let pp_aux_hypo tab a =
  print_sep_spec tab ;
  match a with
  | Dhypothesis(h) -> pp "$d\n" ; print_sep tab "|\n" ; pp_aux_dhypo (tab@["|\t"]) h
  | Fhypothesis(h) -> pp "$f\n" ; print_sep tab "|\n" ; pp_aux_fhypo (tab@["|\t"]) h
  | Ehypothesis(h) -> pp "$e\n" ; print_sep tab "|\n" ; pp_aux_ehypo (tab@["|\t"]) h

let pp_aux_assert tab a =
  print_sep_spec tab ;
  match a with
  | Aassertion(s)    ->
     pp "$a\n" ; print_sep tab "|\n" ; pp_aux_aassert (tab@["|\t"]) s
  | Passertion(s, p) ->
     pp "$p\n" ; print_sep tab "|\n" ; pp_aux_passert (tab@["|\t"]) s p

let rec pp_in_block tab (a : in_block) = match a with
  | BComm(c)   -> pp_aux_comm tab c
  | BVdecl(v)  -> print_sep_spec tab ; pp "$v\n" ; print_sep tab "|\n" ;
                  pp_aux_vdecl (tab@["|\t"]) v
  | BHypo(h)   -> pp_aux_hypo tab h
  | BAssert(s) -> pp_aux_assert tab s
  | BBdecl(b)  -> print_sep_spec tab ; pp "${\n" ; print_sep tab "|\n" ;
                  pp_aux_bdecl (tab@["|\t"]) b
and pp_aux_bdecl tab (a : b_decl) = List.iter (pp_in_block tab) a

let pp_aux_decl tab a =
  print_sep_spec tab ;
  match a with
  | Cdeclaration(c) -> pp "$c\n" ; print_sep tab "|\n" ; pp_aux_cdecl (tab@["|\t"]) c
  | Vdeclaration(v) -> pp "$v\n" ; print_sep tab "|\n" ; pp_aux_vdecl (tab@["|\t"]) v
  | Bdeclaration(b) -> pp "${\n" ; print_sep tab "|\n" ; pp_aux_bdecl (tab@["|\t"]) b
