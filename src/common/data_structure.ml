open C_types
open Error

(** Module Set of a file's v-declarations. *)
module V_Decl = Map.Make(String)

(** Module Set of a file's c-declarations. *)
module C_Decl = Set.Make(String)

module M = Map.Make(Int)





(** [is_var_in_fh v fh_data] indicates if a variable [v] is linked to
    a f-hypothesis in the f-hypothesis structure [fh_data] or not. *)
let is_var_in_fh v (fh_data : f_hypo M.t) = (* type more general btw *)
  let f _ (_, _, var) res = (v = var) || res in M.fold f fh_data false

(** [get_var_map v fh_data] returns the unique f-hypothesis in [fh_data] associated
    with the variable [v], ONLY IF THE HYPOTHESIS EXISTS IN [fh_data]. *)
let get_var_type v fh_data =
  let f key ((_, _, var) as elt) res = if v = var then (key, elt) else res in
  M.fold f fh_data (0, ("", "", "")) (* TODO Elliot - what's happened if v is not in hst? *)

(** [eh_to_fh_map eh fh_data new_fh_data] returns the list of
    fh_set's f-hypotheses linked to variables associated with
    the e-hypothesis [eh]. *) (* TODO Elliot - fix renaming *)
let eh_to_fh_map (eh : e_hypo) fh_data new_fh_data =
  let (_, _, mms) = eh in
  let rec aux mms res = match mms with
    | [] -> res
    | ms::t ->
       if is_var_in_fh ms fh_data then
         let (k,f) = get_var_type ms fh_data in
         aux t (M.add k f res)
       else aux t res
  in
  aux mms new_fh_data

(* TODO Elliot - doc *)
let toto fh_data typ type_cte_data special_cte_data mms new_fh_data v_sem v_not_sem =
  let rec aux mms new_fh_data = match mms with
    | [] ->
       let fh_l_res = List.map (fun (_,b) -> b) (M.bindings new_fh_data) in
       if not(C_Decl.mem typ type_cte_data) && typ != "(" && typ != ")"
       then ( v_sem     fh_l_res, (C_Decl.add typ special_cte_data) )
       else ( v_not_sem fh_l_res, special_cte_data )
    | ms::t ->
       if is_var_in_fh ms fh_data then
         let (k,f) = get_var_type ms fh_data in
         aux t (M.add k f new_fh_data)
       else
         aux t new_fh_data
  in
  aux mms new_fh_data



(**The two following functions add a variable contained in a v declaration v_d ; or a constant contained in a c declaration c_d is a variables map v_d_map
   or a constants set c_d_set, and raise an error if any duplicate exists in the first case. *) (* TODO need to be readable *)

let rec fill_c_set c_d c_d_set =
  let f ms x =
    if C_Decl.mem ms c_d_set then
      (* raise (MM2DKError(IllFormedMetamathFile, "normalization.ml", "fill_c_set",
                        "MMFileError - Multiple constant declaration error.")) *)
      (wrn_msg _STDOUT "MMFileError - Multiple constant declaration error." ; x)
    else x
  in
  match c_d with
  | []    -> C_Decl.empty (* failwith "Arg" *)
  | [ms]  -> f ms (C_Decl.add ms c_d_set)
  | ms::t -> f ms (fill_c_set t (C_Decl.add ms c_d_set))

let rec fill_v_map v_d v_d_map =
  let f ms = if V_Decl.mem ms v_d_map then v_d_map else (V_Decl.add ms None v_d_map) in
  match v_d with
  | []    -> V_Decl.empty (* failwith "Arg" *)
  | [ms]  -> f ms
  | ms::t -> fill_v_map t (f ms)
