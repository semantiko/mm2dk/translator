open C_types
open Error

let p (s : string) = pp _STDOUT "%s " s
let p_end () = pp _STDOUT "$.\n"

let pp_import import =
  p "$[" ; List.iter p import ; pp _STDOUT "$]\n"

let pp_mms mms = List.iter (fun s -> p s) mms

let pp_aux x keyword =
  let rec aux = function
    | [] -> failwith "InternalError"
    | [m] -> p m
    | m::decl -> (p m ; aux decl)
  in
  match x with
  | []   -> ()
  | _::_ -> p keyword ; aux x ; p_end ()

let pp_constant c = pp_aux c "$c"
let pp_variable v = pp_aux v "$v"

let pp_disjoint disj =
  let rec aux d = match d with
    | DhypoOne(v1,v2) -> p v1  ; p v2
    | DhypoMany(d,v)  -> aux d ; p v
  in
  p "$d" ; aux disj ; p_end ()

let pp_floating (label, tc, v) =
  p label ; p "$f" ; p tc ; p v ; p_end ()

let pp_essential (label, tc, mms) =
  p label ; p "$e" ; p tc ; pp_mms mms ; p_end ()

let pp_hypothesis = function
  | Dhypothesis d -> pp_disjoint  d
  | Fhypothesis f -> pp_floating  f
  | Ehypothesis e -> pp_essential e

let pp_statement (label, tc, mms) b =
  p label ; if b then p "$a" else p "$p" ; p tc ; pp_mms mms

let pp_proof proof =
  p "$=" ;
  let f = function
    | Qmark(q) -> pp _STDOUT "%c " q
    | Label(l) -> p l
  in
  List.iter f proof

let pp_assertion asser =
  (match asser with
   | Aassertion(s)    -> pp_statement s true
   | Passertion(s, p) -> pp_statement s false ; pp_proof p) ; p_end ()

let rec iter_space n = if n = 0 then pp _STDOUT "  " else iter_space (n-1)

let rec pp_block block n =
  let aux block = match block with
   | BComm(_)   -> iter_space n
   | BVdecl(v)  -> iter_space n ; pp_variable v
   | BBdecl(b)  -> iter_space n ; pp_block b (n+1)
   | BHypo(hyp) -> iter_space n ; pp_hypothesis hyp
   | BAssert(a) -> iter_space n ; pp_assertion  a
  in
  pp _STDOUT "${\n" ; List.iter aux block ; pp _STDOUT "$}\n"

let pp_declaration = function
  | Cdeclaration c -> pp_constant c
  | Vdeclaration v -> pp_variable v
  | Bdeclaration b -> pp_block b 0
