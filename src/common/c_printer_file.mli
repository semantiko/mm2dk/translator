open C_types

val pp_import : import -> unit

val pp_constant : c_decl -> unit
val pp_variable : v_decl -> unit

val pp_floating   : f_hypo -> unit
val pp_essential  : e_hypo -> unit
val pp_disjoint   : d_hypo -> unit
val pp_hypothesis : hypothesis -> unit

val pp_statement : statement -> bool -> unit
val pp_proof     : proof -> unit
val pp_assertion : assertion -> unit

val pp_declaration : declaration -> unit
