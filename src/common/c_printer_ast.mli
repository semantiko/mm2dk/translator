open C_types

val pp : ('a, out_channel, unit) format -> 'a

val print_sep : string list -> string -> unit

val print_sep_spec : string list -> unit

val pp_aux_comm : string list -> comment -> unit

val pp_aux_import : string list -> import -> unit

val pp_aux_hypo : string list -> hypothesis -> unit

val pp_aux_decl : string list -> declaration -> unit

val pp_aux_assert : string list -> assertion -> unit
