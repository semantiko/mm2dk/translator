open MM_common.Error
open Norm
open Terminal.Command_line
open MM_common.C_printer_file

let _ =
  (* STEP A: Parse the command-line *)
  try
    if Sys.argv.(1) = "" then ()
    else
      (Terminal.Command_line.parse () ;
       (* STEP B: Do lexing and parsing *)
       let lexbuf = Lexing.from_channel (!Terminal.Command_line.input)  in
       let ast = Parsing.MM_parser.file (Parsing.MM_lexer.token) lexbuf in
       if !parsing_only then
         if !pp_ast then Parsing.P_printer_ast.print_ast ast else Parsing.P_printer_file.pp_file ast
       else
       if not(!shallow)
       then
         (let sigma, ax, th = fst (Normalization_bis.norm ast) in
          if !normalize_only
          then (pp_constant sigma ; Normalization_bis.pp_axiom ax ; Normalization_bis.pp_theorem th)
          else
            ((* Creating the Dialekto formatter to print the translated file *)
              let l = String.length (!Terminal.Command_line.filename) in
              let f_name = (String.sub (!Terminal.Command_line.filename) 0 (l-3)) in
              let file = open_out (f_name ^ ".lp") in
              let ppc = Format.formatter_of_out_channel file in
              (* Print the prelude *)
              Deep.Encoding.pp_prelude ppc ;
              (* Print the signature *)
              List.iter (Deep.Encoding.pp_signature ppc Printing.Renaming.name) sigma ;
              (* Print the axioms *)
              Deep.Encoding.pp_axiom ppc Printing.Renaming.name ax ;
              (* Print the theorems *)
              Deep.Encoding.pp_th ppc Printing.Renaming.name ax th))
       else
         (* STEP C: Normalization *)
         (let ((c_ty, c_sp, c_op, _, pre_n) as n_f, fh_m) =
            try Normalization.normalize_file ast
            with MM2DKError(_, fileN, funcN, m) -> err_3 _STDOUT "%s (Function %s in the file %s)." m funcN fileN ; failwith ""
          in
          if !normalize_only then
            if !pp_ast then N_printer_ast.print_normalized_ast n_f else N_printer_file.pp_n_file n_f
          else
            (* Creating the Dialekto formatter to print the translated file *)
            let l = String.length (!Terminal.Command_line.filename) in
            let f_name = (String.sub (!Terminal.Command_line.filename) 0 (l-3)) in
            let file = open_out (f_name ^ ".lp") in
            let ppc = Format.formatter_of_out_channel file in
            let pp_LP = Presilo.Lp.pp_lp_command (fun i -> i) ppc in

            (* Get additionnal properties of operators in the associated JSON file *)
            let add_prop = Shallow.Json_running.get_JSON_data f_name in

            (* STEP D: Translation *)
            try
              let asser_file = Shallow.Process.translation c_op pre_n fh_m add_prop in

              (* Translated types and operators declarations *)

              (* A. Translate some constants into types in Dedukti. *)
              let types_c   = Shallow.Axiomatization.from_constant_to_type c_ty in

              (* B. Translate some constants into symbols in Dedukti. *)
              let spe_op_c  = Shallow.Axiomatization.write_sp_ope c_sp add_prop in

              (* STEP E: Printing *)
              List.iter pp_LP types_c  ;
              List.iter pp_LP spe_op_c ;
              List.iter pp_LP asser_file
            with MM2DKError(_, fileN, funcN, m) -> err_3 _STDOUT "%s (Function %s in the file %s)." m funcN fileN))
  with MM2DKError(UserError, fileN, funcN, m) -> err_3 _STDOUT "User Error - %s (Function %s in the file %s)." m funcN fileN
     | Invalid_argument(_) -> err_msg _STDOUT "User Error - No file was given thanks to the command-line."
