(** References for managing the options *)
val pp_ast         : bool ref
val parsing_only   : bool ref
val normalize_only : bool ref
val shallow        : bool ref
val filename       : string ref
val input          : in_channel ref

(** [parse ()] parses the command line. *)
val parse : unit -> unit
