open Arg

open MM_common.Error
open Norm.Normalization
open Shallow.Lambda_term

(** References for managing the options *)
let pp_ast           = ref false
let parsing_only     = ref false
let normalize_only   = ref false
let shallow          = ref false
let filename         = ref ""
let input            = ref stdin

(** Utilities *)

(** [check_extension s] checks that the input file has the
    extension ".mm". *)
let check_extension s =
  let len = String.length s in
  if len > 3 then
    (if not (String.sub s ((String.length s)-3) 3 = ".mm")
     then
       raise (MM2DKError (UserError, "command_line.ml", "check_extension", "Expected extension: .mm")))
  else
    raise (MM2DKError (UserError, "command_line.ml", "check_extension", "Name file very short."))

(** [parse ()] parses the command line. *)
let parse : unit -> unit = fun () ->
  let usage_msg =
    "usage: ./main [--no-comment | --parsing-only | --normalize-only] [--debug-symbol] [--debug-substitution] [--no-color] metamath_file"
  in
  parse
    [("--pp-ast",             Unit (fun () -> pp_ast:=true),
      "Print the Metamath AST.");
     ("--parsing-only",       Unit (fun () -> parsing_only:=true),
      "Stop the process just after parsing.");
     ("--normalize-only",     Unit (fun () -> normalize_only:=true),
      "Stop the process just after normalizing.");
     ("--shallow",     Unit (fun () -> shallow:=true),
      "Use the shallow encoding.");
     ("--debug-symbol",       Unit (fun () -> debug_symbol:=true),
      "Print the Metamath signature.");
     ("--debug-substitution", Unit (fun () -> debug_substitution:=true),
      "Print the substitution during proof generation process.");
     ("--no-color",           Unit (fun () -> no_color:=true),
      "Disable the colors in the terminal.")]
    (fun s ->
       (check_extension s ;
        filename := s ;
        try
          input := open_in s
        with Sys_error(e) -> raise (MM2DKError (UserError, "command_line.ml", "parse", e))))
    ("During the translation of a Metamath file:\n" ^ usage_msg)
