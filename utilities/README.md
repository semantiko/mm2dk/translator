This folder is composed of many scripts:
 - `get_metamath-exe.sh`: to get the current Metamath executable
 - `test_examples.sh`: to test the Metamath example repository
 - `MM-uncompressed.sh`: to uncompress a Metamath file or all Metamath files in a folder
 - `get_one_proof.sh`: to generate a Metamath file that contains a proof with all its dependencies
