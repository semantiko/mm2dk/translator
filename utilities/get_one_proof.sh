#! /bin/bash -e

proof_file=$1 # The file where the proof is
proof_name=$2 # The proof to extract
MM_translator=metamath

# usage: ./get_one_proof proof_file proof_name

./$MM_translator "read '$proof_file'" "write source '$proof_name.mm' / extract $proof_name" exit
./$MM_translator "read '$proof_name.mm'" "verify proof *" exit
