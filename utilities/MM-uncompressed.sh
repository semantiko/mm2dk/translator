#! /bin/bash -e

MM_translator=metamath
translator_name=main
folder=uncompressed-file

# usage: ./MM-uncompressed file-or-folder

mkdir -p $folder
if [ -f $1 ]; then
   ./$MM_translator "read '$1'" 'save proof * /normal' "write source '$folder/$1'" exit > /dev/null
   ./$translator_name $1
else
  mkdir -p $folder/$1
  for d in $(find $1 -type d ) ; do
    mkdir -p $folder/$d
  done
  for f in $(find $1 -mindepth 1 -type f | sort -d ) ; do
      if [ $(du -b $f | awk '{ print $1 }') -le 100000000 ]; then # <= 100 Mo
         ./$MM_translator "read '$f'" 'save proof * /normal' "write source '$folder/$f'" exit #> /dev/null
      else
        echo "WARNING: The file $f wasn't uncompressed because his size is greater than 100 Mo."
      fi
  done
fi
