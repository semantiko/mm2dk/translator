#! /bin/bash -e

git clone https://github.com/metamath/metamath-exe
cd metamath-exe/src
gcc m*.c -o metamath
mv metamath ../..
cd ../..
rm -rf metamath-exe
