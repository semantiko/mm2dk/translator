$c -> -. ( ) wff |- $.
$v phi psi chi $.

wph $f wff phi $.
wps $f wff psi $.
wch $f wff chi $.

wn $a wff -. psi $.
wi $a wff ( phi -> psi ) $.

a1 $a |- ( phi -> ( psi -> phi ) ) $.
a2 $a |- ( ( phi -> ( psi -> chi ) ) -> ( ( phi -> psi ) -> ( phi -> chi ) ) ) $.
a3 $a |- ( -. phi -> -. psi ) -> ( psi -> phi ) $.

${ min $e |- phi $.
   maj $e |- ( phi -> psi ) $.
   mp  $a |- psi $.
$}

${  hyp $e |- ( phi -> ( psi -> chi ) ) $.
    a2i $p |- ( ( phi -> psi ) -> ( phi -> chi ) )
      $= wph wps wch wi wi
         wph wps wi wph wch wi wi
         hyp
         wph wps wch a2
         mp $.
$}
