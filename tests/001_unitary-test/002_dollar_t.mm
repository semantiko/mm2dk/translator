
$(
#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#
       Appendix:  Typesetting definitions for the tokens in this file
#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#
$)

$c 0 A $.
$v x $.

type-x $f A x $.

type-zero $a A 0 $.

$( $t

/* The '$ t' (no space between '$' and 't') token indicates the beginning
    of the typesetting definition section, embedded in a Metamath
    comment.  There may only be one per source file, and the typesetting
    section ends with the end of the Metamath comment.  The typesetting
    section uses C-style comment delimiters.  TODO:  Allow multiple
    typesetting comments */

/* These are the LaTeX and HTML definitions in the order the tokens are
    introduced in $c or $v statements.  See HELP TEX or HELP HTML in the
    Metamath program. */

/* Note that the ALT= fields in htmldefs should be preceded by a space.  This
   ensures that a screen copy from the web page will have a space between
   symbols. */
/* Math font table with XITS and LaTeX defs:
   http://meeting.contextgarden.net/2011/talks/day3_05_ulrik_opentype/Samples/
           unimath-symbols.pdf */


/******* Web page format settings *******/

/* Custom CSS for Unicode fonts */
/* The woff font file was obtained from
   http://fred-wang.github.io/MathFonts/XITS/xits-math.woff 28-Aug-2015 */
htmlcss '<STYLE TYPE="text/css">\n' +
    '<!--\n' +
    '  .setvar { color: red; }\n' +
    '  .wff { color: blue; }\n' +
    '  .class { color: #C3C; }\n' +
    '  .symvar { border-bottom:1px dotted;color:#C3C}\n' +
    '  .typecode { color: gray }\n' +
    '  .hidden { color: gray }\n' +
    '  @font-face {\n' +
    '    font-family: XITSMath-Regular;\n' +
    '    src: url(xits-math.woff);\n' +
    '  }\n' +
    '  .math { font-family: XITSMath-Regular }\n' +
    '-->\n' +
    '</STYLE>\n' +
    '<LINK href="mmset.css" title="mmset"\n' +
    '    rel="stylesheet" type="text/css">\n' +
    '<LINK href="mmsetalt.css" title="mmsetalt"\n' +
    '    rel="alternate stylesheet" type="text/css">';

/* Tag(s) for the main SPAN surrounding all Unicode math */
htmlfont 'CLASS=math';

/* Page title, home page link */
htmltitle "Metamath Proof Explorer";
htmlhome '<A HREF="mmset.html"><FONT SIZE=-2 FACE=sans-serif>' +
    '<IMG SRC="mm.gif" BORDER=0 ALT='  +
    '"Home" HEIGHT=32 WIDTH=32 ALIGN=MIDDLE STYLE="margin-bottom:0px">' +
    'Home</FONT></A>';
/* Optional file where bibliographic references are kept */
/* If specified, e.g. "mmset.html", Metamath will hyperlink all strings of the
   form "[rrr]" (where "rrr" has no whitespace) to "mmset.html#rrr" */
/* A warning will be given if the file "mmset.html" with the bibliographical
   references is not present.  It is read in order to check existence of
   the references. */
htmlbibliography "mmset.html";

/* Page title, home page link */
/* These are the variables used for the Hilbert Space extension to
   set.mm. */
exthtmltitle "Hilbert Space Explorer";
exthtmlhome '<A HREF="mmhil.html"><FONT SIZE=-2 FACE=sans-serif>' +
    '<IMG SRC="atomic.gif" BORDER=0 ALT='  +
    '"Home" HEIGHT=32 WIDTH=32 ALIGN=MIDDLE STYLE="margin-bottom:0px">' +
    'Home</FONT></A>';
/* The variable "exthtmllabel" means that all states including
   and after this label should use the "ext..." variables. */
exthtmllabel "chil";
/* A warning will be given if the file with the bibliographical references
   is not present. */
exthtmlbibliography "mmhil.html";

/* Variable color key at the bottom of each proof table */
htmlvarcolor '<SPAN CLASS=wff STYLE="color:blue;font-style:normal">wff</SPAN> '
    + '<SPAN CLASS=setvar STYLE="color:red;font-style:normal">setvar</SPAN> '
    + '<SPAN CLASS=class STYLE="color:#C3C;font-style:normal">class</SPAN>';

/* GIF and Unicode HTML directories - these are used for the GIF version to
   crosslink to the Unicode version and vice-versa */
htmldir "../mpegif/";
althtmldir "../mpeuni/";

/* Optional link(s) to other versions of the theorem page.  A "*" is replaced
   with the label of the current theorem.  If you need a literal "*" as part
   of the URL, use the alternate URL encoding "%2A". */
htmlexturl '<A HREF="http://metamath.tirix.org/*.html">'
    + 'Structured version</A>&nbsp;&nbsp; '
    + '<A HREF="https://expln.github.io/metamath/asrt/*.html">'
    + 'Visualization version</A>&nbsp;&nbsp; ';


/******* Symbol definitions *******/

htmldef "(" as "<IMG SRC='lp.gif' WIDTH=5 HEIGHT=19 ALT=' (' TITLE='('>";
  althtmldef "(" as "(";
  latexdef "(" as "(";
htmldef ")" as "<IMG SRC='rp.gif' WIDTH=5 HEIGHT=19 ALT=' )' TITLE=')'>";
  althtmldef ")" as ")";
  latexdef ")" as ")";
htmldef "E." as
    "<IMG SRC='exists.gif' WIDTH=9 HEIGHT=19 ALT=' E.' TITLE='E.'>";
  althtmldef "E." as '&exist;'; /* &#8707; */
    /* Without sans-serif, bad in Opera and way too big in FF3 */
    /* 2-Jan-2016 reverted sans-serif */
  latexdef "E." as "\exists";
htmldef "F/" as
    "<IMG SRC='finv.gif' WIDTH=9 HEIGHT=19 ALT=' F/' TITLE='F/'>";
  althtmldef "F/" as "&#8498;";
  latexdef "F/" as "\Finv";
htmldef "class" as
    "<IMG SRC='_class.gif' WIDTH=32 HEIGHT=19 ALT=' class' TITLE='class'> ";
  althtmldef "class" as
    '<SPAN CLASS=typecode STYLE="color:gray">class </SPAN>';
  latexdef "class" as "{\rm class}";
htmldef "=" as " <IMG SRC='eq.gif' WIDTH=12 HEIGHT=19 ALT=' =' TITLE='='> ";
  althtmldef "=" as ' = '; /* &equals; */
  latexdef "=" as "=";
htmldef "e." as " <IMG SRC='in.gif' WIDTH=10 HEIGHT=19 ALT=' e.' TITLE='e.'> ";
  althtmldef "e." as ' &isin; ';
    /* 2-Jan-2016 reverted sans-serif */
  latexdef "e." as "\in";
htmldef "[" as "<IMG SRC='lbrack.gif' WIDTH=5 HEIGHT=19 ALT=' [' TITLE='['>";
  althtmldef "[" as '['; /* &lsqb; */
  latexdef "[" as "[";
htmldef "/" as
    " <IMG SRC='solidus.gif' WIDTH=6 HEIGHT=19 ALT=' /' TITLE='/'> ";
  althtmldef "/" as ' / '; /* &sol; */
  latexdef "/" as "/";
htmldef "]" as "<IMG SRC='rbrack.gif' WIDTH=5 HEIGHT=19 ALT=' ]' TITLE=']'>";
  althtmldef "]" as ']'; /* &rsqb; */
  latexdef "]" as "]";
htmldef "E!" as "<IMG SRC='_e1.gif' WIDTH=12 HEIGHT=19 ALT=' E!' TITLE='E!'>";
  althtmldef "E!" as '&exist;!';
    /* 2-Jan-2016 reverted sans-serif */
  latexdef "E!" as "\exists{!}";
htmldef "E*" as "<IMG SRC='_em1.gif' WIDTH=15 HEIGHT=19 ALT=' E*' TITLE='E*'>";
  althtmldef "E*" as '&exist;*';
    /* 2-Jan-2016 reverted sans-serif */
  latexdef "E*" as "\exists^\ast";
htmldef "{" as "<IMG SRC='lbrace.gif' WIDTH=6 HEIGHT=19 ALT=' {' TITLE='{'>";
  althtmldef "{" as '{'; /* &lcub; */
  latexdef "{" as "\{";
htmldef "|" as " <IMG SRC='vert.gif' WIDTH=3 HEIGHT=19 ALT=' |' TITLE='|'> ";
  althtmldef "|" as ' &#8739; '; /* &vertbar; */
    /* 2-Jan-2016 reverted sans-serif */
  latexdef "|" as "|";
htmldef "}" as "<IMG SRC='rbrace.gif' WIDTH=6 HEIGHT=19 ALT=' }' TITLE='}'>";
  althtmldef "}" as '}'; /* &rcub; */
  latexdef "}" as "\}";
htmldef "F/_" as
    "<IMG SRC='_finvbar.gif' WIDTH=9 HEIGHT=19 ALT=' F/_' TITLE='F/_'>";
  althtmldef "F/_" as "<U>&#8498;</U>";
  latexdef "F/_" as "\underline{\Finv}";
htmldef "CondEq" as "CondEq";
  althtmldef "CondEq" as "CondEq";
  latexdef "CondEq" as "\mbox{CondEq}";
htmldef "./\" as
    " <IMG SRC='_.wedge.gif' WIDTH=11 HEIGHT=19 ALT=' ./\' TITLE='./\'> ";
  althtmldef "./\" as
    ' <SPAN CLASS=symvar STYLE="border-bottom:1px dotted;color:#C3C">' +
    '&and;</SPAN> ';
  latexdef "./\" as "\wedge";
htmldef ".\/" as
    " <IMG SRC='_.vee.gif' WIDTH=11 HEIGHT=19 ALT=' .\/' TITLE='.\/'> ";
  althtmldef ".\/" as
    ' <SPAN CLASS=symvar STYLE="border-bottom:1px dotted;color:#C3C">' +
    '&or;</SPAN> ';
  latexdef ".\/" as "\vee";
htmldef ".<_" as
    " <IMG SRC='_.le.gif' WIDTH=11 HEIGHT=19 ALT=' .&lt;_' TITLE='.&lt;_'> ";
  althtmldef ".<_" as
    ' <SPAN CLASS=symvar STYLE="border-bottom:1px dotted;color:#C3C">' +
    '&le;</SPAN> ';
  latexdef ".<_" as "\le";
htmldef ".<" as     /* Symbol as variable */
    " <IMG SRC='_.lt.gif' WIDTH=11 HEIGHT=19 ALT=' .&lt;' TITLE='.&lt;'> ";
  althtmldef ".<" as
    /* This is how to put a dotted box around the symbol: */
    /* border means box around symbol; border-bottom underlines symbol */
    ' <SPAN CLASS=symvar STYLE="border-bottom:1px dotted;color:#C3C">' +
    '&lt;</SPAN> ';
    /* TODO: can this STYLE sequence be done with a CLASS? */
    /* Move the underline down 3px so it isn't too close to symbol */
    /*
    ' <SPAN STYLE="vertical-align:-3px">' +
    '<SPAN CLASS=symvar STYLE="text-decoration:underline dotted;color:#C3C">' +
    '<SPAN STYLE="vertical-align:3px">&lt;</SPAN></SPAN></SPAN> ';
    */
  latexdef ".<" as "<";
/*
htmldef "iota" as
    "<IMG SRC='iota.gif' WIDTH=6 HEIGHT=19 ALT=' iota' TITLE='iota'>";
  althtmldef "iota" as '<FONT SIZE="+1">&iota;</FONT>';
  latexdef "iota" as "\iota";
*/
/* 30-Nov-2013 changed to rotated iota */
htmldef "iota" as
    "<IMG SRC='riota.gif' WIDTH=6 HEIGHT=19 ALT=' iota' TITLE='iota'>";
  althtmldef "iota" as '&#8489;';
  latexdef "iota" as "\mathrm{\rotatebox[origin=C]{180}{$\iota$}}";
htmldef "Smo" as
    "<IMG SRC='_smo.gif' WIDTH=27 HEIGHT=19 ALT=' Smo' TITLE='Smo'> ";
  althtmldef "Smo" as "Smo ";
  latexdef "Smo" as "{\rm Smo}";
htmldef "recs" as "recs";
  althtmldef "recs" as "recs";
  latexdef "recs" as "\mathrm{recs}";
htmldef "rec" as
    "<IMG SRC='_rec.gif' WIDTH=21 HEIGHT=19 ALT=' rec' TITLE='rec'>";
  althtmldef "rec" as 'rec';
  latexdef "rec" as "{\rm rec}";
htmldef "seqom" as "seq<SUB>&#x1D714;</SUB>";
  althtmldef "seqom" as "seq<SUB>&#x1D714;</SUB>";
  latexdef "seqom" as "{\rm seqom}";
htmldef "1o" as "<IMG SRC='_1o.gif' WIDTH=13 HEIGHT=19 ALT=' 1o' TITLE='1o'>";
  althtmldef "1o" as '1<SUB>&#x1D45C;</SUB>';
  latexdef "1o" as "1_o";
htmldef "2o" as "<IMG SRC='_2o.gif' WIDTH=14 HEIGHT=19 ALT=' 2o' TITLE='2o'>";
  althtmldef "2o" as '2<SUB>&#x1D45C;</SUB>';
  latexdef "2o" as "2_o";
htmldef "3o" as "<IMG SRC='_3o.gif' WIDTH=14 HEIGHT=19 ALT=' 3o' TITLE='3o'>";
  althtmldef "3o" as "3<SUB>&#x1D45C;</SUB>"; latexdef "3o" as "3_o";
htmldef "4o" as "<IMG SRC='_4o.gif' WIDTH=15 HEIGHT=19 ALT=' 4o' TITLE='4o'>";
  althtmldef "4o" as "4<SUB>&#x1D45C;</SUB>"; latexdef "4o" as "4_o";
htmldef "+o" as
    " <IMG SRC='_plo.gif' WIDTH=18 HEIGHT=19 ALT=' +o' TITLE='+o'> ";
  althtmldef "+o" as ' +<SUB>&#x1D45C;</SUB> ';
  latexdef "+o" as "+_o";
htmldef ".o" as
    " <IMG SRC='_cdo.gif' WIDTH=10 HEIGHT=19 ALT=' .o' TITLE='.o'> ";
  althtmldef ".o" as ' &middot;<SUB>&#x1D45C;</SUB> ';
  latexdef ".o" as "\cdot_o";
htmldef "^o" as
    " <IMG SRC='_hato.gif' WIDTH=11 HEIGHT=19 ALT=' ^o' TITLE='^o'> ";
  althtmldef "^o" as ' &uarr;<SUB>&#x1D45C;</SUB> ';
  latexdef "^o" as "\uparrow_o"; /*
  latexdef "^o" as "\hat{\ }_o"; */
htmldef "Er" as
    " <IMG SRC='_er.gif' WIDTH=16 HEIGHT=19 ALT=' Er' TITLE='Er'> ";
  althtmldef "Er" as ' Er ';
  latexdef "Er" as "{\rm Er}";
htmldef "/." as
    "<IMG SRC='diagup.gif' WIDTH=14 HEIGHT=19 ALT=' /.' TITLE='/.'>";
  althtmldef "/." as ' <B>/</B> ';
  latexdef "/." as "\diagup";
htmldef "^m" as
    " <IMG SRC='_hatm.gif' WIDTH=15 HEIGHT=19 ALT=' ^m' TITLE='^m'> ";
  althtmldef "^m" as ' &uarr;<SUB>&#x1D45A;</SUB> ';
  latexdef "^m" as "\uparrow_m";
htmldef "^pm" as
    " <IMG SRC='_hatpm.gif' WIDTH=21 HEIGHT=19 ALT=' ^pm' TITLE='^pm'> ";
  althtmldef "^pm" as ' &uarr;<SUB><I>pm</I></SUB> ';
  latexdef "^pm" as "\uparrow_{pm}";
htmldef "X_" as
    "<IMG SRC='_bigtimes.gif' WIDTH=11 HEIGHT=19 ALT=' X_' TITLE='X_'>";
  althtmldef "X_" as '<FONT SIZE="+1">X</FONT>';
    /* 2-Jan-2016 reverted sans-serif */
  latexdef "X_" as "\mbox{\large\boldmath$\times$}";
htmldef "~~" as
    " <IMG SRC='approx.gif' WIDTH=13 HEIGHT=19 ALT=' ~~' TITLE='~~'> ";
  althtmldef "~~" as ' &#8776; '; /* &ap; */
  latexdef "~~" as "\approx";
htmldef "~<_" as
   " <IMG SRC='preccurlyeq.gif' WIDTH=11 HEIGHT=19 " +
    "ALT=' ~&lt;_' TITLE='~&lt;_'> ";
  althtmldef "~<_" as ' &#8828; '; /* &prcue; */
    /* 2-Jan-2016 reverted sans-serif */
  latexdef "~<_" as "\preccurlyeq";
htmldef "~<" as
    " <IMG SRC='prec.gif' WIDTH=11 HEIGHT=19 ALT=' ~&lt;' TITLE='~&lt;'> ";
  althtmldef "~<" as ' &#8826; '; /* &pr; */
    /* 2-Jan-2016 reverted sans-serif */
  latexdef "~<" as "\prec";
htmldef "Fin" as
    "<IMG SRC='_fin.gif' WIDTH=21 HEIGHT=19 ALT=' Fin' TITLE='Fin'>";
  althtmldef "Fin" as 'Fin';
  latexdef "Fin" as "{\rm Fin}";
htmldef "finSupp" as ' finSupp ';
  althtmldef "finSupp" as ' finSupp ';
  latexdef "finSupp" as "{\rm finSupp}";
htmldef "Undef" as
    "<IMG SRC='_undef.gif' WIDTH=39 HEIGHT=19 ALT=' Undef' TITLE='Undef'>";
  althtmldef "Undef" as "Undef";
  latexdef "Undef" as "{\rm Undef}";
htmldef "iota_" as
    "<IMG SRC='_riotabar.gif' WIDTH=6 HEIGHT=19 ALT=' iota_' TITLE='iota_'>";
  althtmldef "iota_" as '<U>&#8489;</U>';
  latexdef "iota_" as
      "\underline{\mathrm{\rotatebox[origin=C]{180}{$\iota$}}}";
htmldef "fi" as
    "<IMG SRC='_fi.gif' WIDTH=10 HEIGHT=19 ALT=' fi' TITLE='fi'>";
  althtmldef "fi" as "fi";
  latexdef "fi" as "{\rm fi}";
htmldef "sup" as
    "<IMG SRC='_sup.gif' WIDTH=22 HEIGHT=19 ALT=' sup' TITLE='sup'>";
  althtmldef "sup" as 'sup';
  latexdef "sup" as "{\rm sup}";
htmldef "inf" as 'inf';
  althtmldef "inf" as 'inf';
  latexdef "inf" as "{\rm inf}";
htmldef "OrdIso" as "OrdIso";
  althtmldef "OrdIso" as 'OrdIso';
  latexdef "OrdIso" as "{\rm OrdIso}";
/* the standard symbol for this is \aleph, but that collides */
htmldef "har" as "har";
  althtmldef "har" as "har";
  latexdef "har" as "{\rm har}";
htmldef "~<_*" as " <IMG SRC='preccurlyeq.gif' WIDTH=11 HEIGHT=19 " +
   "ALT=' ~&lt;_' TITLE='~&lt;_'><SUP>*</SUP> ";
  althtmldef "~<_*" as ' &#8828;<SUP>*</SUP> ';
  /* &prcue; */
    /* 2-Jan-2016 reverted sans-serif */
  latexdef "~<_*" as "\preccurlyeq^*";
htmldef "CNF" as " CNF ";
  /* surrounding spaces since it is often infixed */
  althtmldef "CNF" as " CNF ";
  latexdef "CNF" as " {\rm CNF} ";
htmldef "TC" as
    "<IMG SRC='_tc.gif' WIDTH=20 HEIGHT=19 ALT=' TC' TITLE='TC'>";
  althtmldef "TC" as "TC";
  latexdef "TC" as "{\rm TC}";
htmldef "R1" as "<IMG SRC='_r1.gif' WIDTH=15 HEIGHT=19 ALT=' R1' TITLE='R1'>";
  althtmldef "R1" as '&#x1D445;<SUB>1</SUB>';
  latexdef "R1" as "R_1";
  latexdef "rmY" as "{\rm rmY}";
htmldef "LFinGen" as "LFinGen";
  althtmldef "LFinGen" as "LFinGen";
  latexdef "LFinGen" as "{\rm LFinGen}";
htmldef "LNoeM" as "LNoeM";
  althtmldef "LNoeM" as "LNoeM";
  latexdef "LNoeM" as "{\rm LNoeM}";
htmldef "LIndF" as " LIndF ";
  althtmldef "LIndF" as " LIndF ";
  latexdef "LIndF" as " {\rm LIndF} ";
htmldef "LIndS" as "LIndS";
  althtmldef "LIndS" as "LIndS";
  latexdef "LIndS" as "{\rm LIndS}";
htmldef "LNoeR" as "LNoeR";
  althtmldef "LNoeR" as "LNoeR";
  latexdef "LNoeR" as "{\rm LNoeR}";
htmldef "ldgIdlSeq" as "ldgIdlSeq";
  althtmldef "ldgIdlSeq" as "ldgIdlSeq";
  latexdef "ldgIdlSeq" as "\mathrm{ldgIdlSeq}";
htmldef "Monic" as
    " <IMG SRC='_monic.gif' WIDTH=38 HEIGHT=19 ALT=' Monic' TITLE='Monic'> ";
  althtmldef "Monic" as " Monic ";
  latexdef "Monic" as "{\rm Monic}";
htmldef "Poly<" as " Poly<sub>&lt;</sub> ";
  althtmldef "Poly<" as " Poly<sub>&lt;</sub> ";
  latexdef "Poly<" as "{\rm Poly<}";
htmldef "degAA" as "deg<SUB>AA</SUB>";
  althtmldef "degAA" as "deg<SUB>AA</SUB>";
  latexdef "degAA" as "{\rm degAA}";
htmldef "degAAOLD" as "deg<SUB>AA</SUB>";
  althtmldef "degAAOLD" as "deg<SUB>AA</SUB>";
  latexdef "degAAOLD" as "{\rm degAA}";
htmldef "minPolyAA" as "minPolyAA";
  althtmldef "minPolyAA" as "minPolyAA";
  latexdef "minPolyAA" as "{\rm minPolyAA}";
htmldef "_ZZ" as "<SPAN STYLE='text-decoration: overline;'>&#8484;</SPAN>";
    /* 2-Jan-2016 reverted sans-serif */
  althtmldef "_ZZ" as
                 "<SPAN STYLE='text-decoration: overline;'>&#8484;</SPAN>";
    /* 2-Jan-2016 reverted sans-serif */
  latexdef "_ZZ" as "{\rm _ZZ}";
htmldef "IntgOver" as "IntgOver";
  althtmldef "IntgOver" as "IntgOver";
  latexdef "IntgOver" as "{\rm IntgOver}";
htmldef "MEndo" as "MEndo";
  althtmldef "MEndo" as "MEndo";
  latexdef "MEndo" as "\mathrm{MEndo}";
htmldef "SubDRing" as "SubDRing";
  althtmldef "SubDRing" as "SubDRing";
  latexdef "SubDRing" as "\mathrm{SubDRing}";
htmldef "Cntr" as "Cntr";
  althtmldef "Cntr" as "Cntr";
  latexdef "Cntr" as "\mathrm{Cntr}";
htmldef "Cntz" as "Cntz";
  althtmldef "Cntz" as "Cntz";
  latexdef "Cntz" as "\mathrm{Cntz}";
htmldef "CytP" as "CytP";
  althtmldef "CytP" as "CytP";
  latexdef "CytP" as "\mathrm{CytP}";
htmldef "TopSep" as "TopSep";
  althtmldef "TopSep" as "TopSep";
  latexdef "TopSep" as "{\rm TopSep}";
htmldef "TopLnd" as "TopLnd";
  althtmldef "TopLnd" as "TopLnd";
  latexdef "TopLnd" as "{\rm TopLnd}";
/* End of Stefan O'Rear's mathbox */

/* Mathbox of Steve Rodriguez */
htmldef "_Cc" as "C<SUB>&#x1D450;</SUB>";
  althtmldef "_Cc" as "C<SUB>&#x1D450;</SUB>";
  latexdef "_Cc" as "{\rm C}_c";
/* End of Steve Rodriguez's mathbox */

/* Mathbox of Andrew Salmon */
/* geometry proposal */
htmldef "+r" as "<IMG SRC='plus.gif' WIDTH=13 HEIGHT=19 ALT=' +' TITLE='+'>" +
    "<IMG SRC='subr.gif' WIDTH=5 HEIGHT=19 ALT='r' TITLE='r'>";
  althtmldef "+r" as "+<SUB>&#x1D45F;</SUB>";
  latexdef "+r" as "+_r";
htmldef "-r" as
    "<IMG SRC='minus.gif' WIDTH=11 HEIGHT=19 ALT=' -' TITLE='-'>" +
    "<IMG SRC='subr.gif' WIDTH=5 HEIGHT=19 ALT='r' TITLE='r'>";
  althtmldef "-r" as "-<SUB>&#x1D45F;</SUB>";
  latexdef "-r" as "-_r";
htmldef ".v" as "<IMG SRC='cdot.gif' WIDTH=4 HEIGHT=19 ALT=' .' TITLE='.'>" +
    "<IMG SRC='subv.gif' WIDTH=6 HEIGHT=19 ALT='v' TITLE='v'>";
  althtmldef ".v" as ".<SUB>&#x1D463;</SUB>";
  latexdef ".v" as "._v";
htmldef "PtDf" as
    "<IMG SRC='_ptdf.gif' WIDTH=31 HEIGHT=19 ALT=' PtDf' TITLE='PtDf'>";
  althtmldef "PtDf" as "PtDf";
  latexdef "PtDf" as "PtDf";
htmldef "RR3" as
    "<IMG SRC='bbr.gif' WIDTH=13 HEIGHT=19 ALT=' RR' TITLE='RR'>" +
    "<IMG SRC='sup3.gif' WIDTH=6 HEIGHT=19 ALT='3' TITLE='3'>";
  althtmldef "RR3" as "RR3";
  latexdef "RR3" as "RR3";
/*
htmldef "plane3" as
    "<IMG SRC='_plane.gif' WIDTH=35 HEIGHT=19 ALT=' plane' TITLE='plane'>" +
    "<IMG SRC='sup3.gif' WIDTH=6 HEIGHT=19 ALT='3' TITLE='3'>";
  althtmldef "plane3" as "plane3";
  latexdef "plane3" as "plane3";
*/
htmldef "line3" as
    "<IMG SRC='_line.gif' WIDTH=23 HEIGHT=19 ALT=' line' TITLE='line'>" +
    "<IMG SRC='sup3.gif' WIDTH=6 HEIGHT=19 ALT='3' TITLE='3'>";
  althtmldef "line3" as "line3";
  latexdef "line3" as "line3";
/* End of Andrew Salmon's mathbox */

/* Mathbox of Jarvin Udandy */
htmldef "jph" as
    '<SPAN CLASS=wff STYLE="color:blue"><I>jph</I></SPAN>';
  althtmldef "jph" as
    '<SPAN CLASS=wff STYLE="color:blue"><I>jph</I></SPAN>';
  latexdef "jph" as "\rm{jph}";
htmldef "jps" as
    '<SPAN CLASS=wff STYLE="color:blue"><I>jps</I></SPAN>';
  althtmldef "jps" as
    '<SPAN CLASS=wff STYLE="color:blue"><I>jps</I></SPAN>';
  latexdef "jps" as "\rm{jps}";
htmldef "jch" as
    '<SPAN CLASS=wff STYLE="color:blue"><I>jch</I></SPAN>';
  althtmldef "jch" as
    '<SPAN CLASS=wff STYLE="color:blue"><I>jch</I></SPAN>';
  latexdef "jch" as "\rm{jch}";
htmldef "jth" as
    '<SPAN CLASS=wff STYLE="color:blue"><I>jth</I></SPAN>';
  althtmldef "jth" as
    '<SPAN CLASS=wff STYLE="color:blue"><I>jth</I></SPAN>';
  latexdef "jth" as "\rm{jth}";
htmldef "jta" as
    '<SPAN CLASS=wff STYLE="color:blue"><I>jta</I></SPAN>';
  althtmldef "jta" as
    '<SPAN CLASS=wff STYLE="color:blue"><I>jta</I></SPAN>';
  latexdef "jta" as "\rm{jta}";
htmldef "jet" as
    '<SPAN CLASS=wff STYLE="color:blue"><I>jet</I></SPAN>';
  althtmldef "jet" as
    '<SPAN CLASS=wff STYLE="color:blue"><I>jet</I></SPAN>';
  latexdef "jet" as "\rm{jet}";
htmldef "jze" as
    '<SPAN CLASS=wff STYLE="color:blue"><I>jze</I></SPAN>';
  althtmldef "jze" as
    '<SPAN CLASS=wff STYLE="color:blue"><I>jze</I></SPAN>';
  latexdef "jze" as "\rm{jze}";
htmldef "jsi" as
    '<SPAN CLASS=wff STYLE="color:blue"><I>jps</I></SPAN>';
  althtmldef "jsi" as
    '<SPAN CLASS=wff STYLE="color:blue"><I>jps</I></SPAN>';
  latexdef "jsi" as "\rm{jsi}";
htmldef "jrh" as
    '<SPAN CLASS=wff STYLE="color:blue"><I>jrh</I></SPAN>';
  althtmldef "jrh" as
    '<SPAN CLASS=wff STYLE="color:blue"><I>jrh</I></SPAN>';
  latexdef "jrh" as "\rm{jrh}";
htmldef "jmu" as
    '<SPAN CLASS=wff STYLE="color:blue"><I>jmu</I></SPAN>';
  althtmldef "jmu" as
    '<SPAN CLASS=wff STYLE="color:blue"><I>jmu</I></SPAN>';
  latexdef "jmu" as "\rm{jmu}";
htmldef "jla" as
    '<SPAN CLASS=wff STYLE="color:blue"><I>jla</I></SPAN>';
  althtmldef "jla" as
    '<SPAN CLASS=wff STYLE="color:blue"><I>jla</I></SPAN>';
  latexdef "jla" as "\rm{jla}";
/* End of Jarvin Udandy's mathbox */

/* Mathbox of Alexander van der Vekens */
htmldef "defAt" as ' defAt ';
 althtmldef "defAt" as ' defAt ';
 latexdef "defAt" as "{\rm defAt}";
htmldef "'''" as '&#39&#39&#39';
 althtmldef "'''" as '&#39&#39&#39';
 latexdef "'''" as "\textquoteright\textquoteright\textquoteright";
htmldef "((" as ' ((';
 althtmldef "((" as ' ((';
 latexdef "((" as " ((";
htmldef "))" as ')) ';
 althtmldef "))" as ')) ';
 latexdef "))" as ")) ";
htmldef "RePart" as 'RePart';
 althtmldef "RePart" as 'RePart';
 latexdef "RePart" as "{\rm RePart}";
htmldef "Even" as ' Even ';
 althtmldef "Even" as ' Even ';
 latexdef "Even" as "{\rm Even}";
htmldef "Odd" as ' Odd ';
 althtmldef "Odd" as ' Odd ';
 latexdef "Odd" as "{\rm Odd}";
htmldef "GoldbachEven" as ' GoldbachEven ';
 althtmldef "GoldbachEven" as ' GoldbachEven ';
 latexdef "GoldbachEven" as "{\rm GoldbachEven}";
htmldef "GoldbachOdd" as ' GoldbachOdd ';
 althtmldef "GoldbachOdd" as ' GoldbachOdd ';
 latexdef "GoldbachOdd" as "{\rm GoldbachOdd}";
htmldef "GoldbachOddALTV" as ' GoldbachOddALTV ';
 althtmldef "GoldbachOddALTV" as ' GoldbachOddALTV ';
 latexdef "GoldbachOddALTV" as "{\rm GoldbachOddALTV}";
htmldef "prefix" as ' prefix ';
 althtmldef "prefix" as ' prefix ';
 latexdef "prefix" as "{\rm prefix}";
/* Graph theory (revised) */
htmldef ".ef" as '.ef';
  althtmldef ".ef" as ".ef";
  latexdef ".ef" as "{\rm ef}";
htmldef "Vtx" as 'Vtx';
 althtmldef "Vtx" as 'Vtx';
 latexdef "Vtx" as "{\rm Vtx}";
htmldef "iEdg" as 'iEdg';
 althtmldef "iEdg" as 'iEdg';
 latexdef "iEdg" as "{\rm iEdg}";
htmldef "UHGraph" as ' UHGraph ';
 althtmldef "UHGraph" as ' UHGraph ';
 latexdef "UHGraph" as "{\rm UHGraph}";
htmldef "USHGraph" as ' USHGraph ';
 althtmldef "USHGraph" as ' USHGraph ';
 latexdef "USHGraph" as "{\rm USHGraph}";
htmldef "UPGraph" as ' UPGraph ';
 althtmldef "UPGraph" as ' UPGraph ';
 latexdef "UPGraph" as "{\rm UPGraph}";
htmldef "UMGraph" as ' UMGraph ';
 althtmldef "UMGraph" as ' UMGraph ';
 latexdef "UMGraph" as "{\rm UMGraph}";
htmldef "Edg" as 'Edg';
 althtmldef "Edg" as 'Edg';
 latexdef "Edg" as "{\rm Edg}";
htmldef "USPGraph" as ' USPGraph ';
 althtmldef "USPGraph" as ' USPGraph ';
 latexdef "USPGraph" as "{\rm USPGraph}";
htmldef "USGraph" as ' USGraph ';
 althtmldef "USGraph" as ' USGraph ';
 latexdef "USGraph" as "{\rm USGraph}";
htmldef "SubGraph" as ' SubGraph ';
 althtmldef "SubGraph" as ' SubGraph ';
 latexdef "SubGraph" as "{\rm SubGraph}";
htmldef "FinUSGraph" as ' FinUSGraph ';
 althtmldef "FinUSGraph" as ' FinUSGraph ';
 latexdef "FinUSGraph" as "{\rm FinUSGraph}";
htmldef "NeighbVtx" as ' NeighbVtx ';
 althtmldef "NeighbVtx" as ' NeighbVtx ';
 latexdef "NeighbVtx" as "{\rm NeighbVtx}";
htmldef "ComplGraph" as 'ComplGraph';
 althtmldef "ComplGraph" as 'ComplGraph';
 latexdef "ComplGraph" as "{\rm ComplGraph}";
htmldef "ComplUSGraph" as 'ComplUSGraph';
 althtmldef "ComplUSGraph" as 'ComplUSGraph';
 latexdef "ComplUSGraph" as "{\rm ComplUSGraph}";
htmldef "UnivVtx" as 'UnivVtx';
 althtmldef "UnivVtx" as 'UnivVtx';
 latexdef "UnivVtx" as "{\rm UnivVtx}";
htmldef "NN0*" as 'NN0*';
 althtmldef "NN0*" as '&#8469;<SUB>0</SUB><SUP>*</SUP>';
 latexdef "NN0*" as "\mathbb{N}_0^*";
htmldef "VtxDeg" as 'VtxDeg';
 althtmldef "VtxDeg" as 'VtxDeg';
 latexdef "VtxDeg" as "{\rm VtxDeg}";
htmldef "RegGraph" as ' RegGraph ';
 althtmldef "RegGraph" as ' RegGraph ';
 latexdef "RegGraph" as "{\rm RegGraph}";
htmldef "RegUSGraph" as ' RegUSGraph ';
 althtmldef "RegUSGraph" as ' RegUSGraph ';
 latexdef "RegUSGraph" as "{\rm RegUSGraph}";
htmldef "EdgWalks" as ' EdgWalks ';
 althtmldef "EdgWalks" as ' EdgWalks ';
 latexdef "EdgWalks" as "{\rm EdgWalks}";
htmldef "1Walks" as "1Walks";
 althtmldef "1Walks" as "1Walks";
 latexdef "1Walks" as "{\rm 1Walks}";
htmldef "UPWalks" as "UPWalks";
 althtmldef "UPWalks" as "UPWalks";
 latexdef "UPWalks" as "{\rm UPWalks}";
htmldef "WalksOn" as "WalksOn";
  althtmldef "WalksOn" as "WalksOn";
  latexdef "WalksOn" as " {\rm WalksOn} ";
htmldef "TrailS" as "TrailS";
  althtmldef "TrailS" as "TrailS";
  latexdef "TrailS" as " {\rm TrailS} ";
htmldef "TrailsOn" as "TrailsOn";
  althtmldef "TrailsOn" as "TrailsOn";
  latexdef "TrailsOn" as " {\rm TrailsOn} ";
htmldef "PathS" as "PathS";
  althtmldef "PathS" as "PathS";
  latexdef "PathS" as " {\rm PathS} ";
htmldef "SPathS" as "SPathS";
  althtmldef "SPathS" as "SPathS";
  latexdef "SPathS" as " {\rm SPathS} ";
htmldef "PathsOn" as "PathsOn";
  althtmldef "PathsOn" as "PathsOn";
  latexdef "PathsOn" as " {\rm PathsOn} ";
htmldef "SPathsOn" as "SPathsOn";
  althtmldef "SPathsOn" as "SPathsOn";
  latexdef "SPathsOn" as "{\rm SPathsOn}";
htmldef "ClWalkS" as "ClWalkS";
 althtmldef "ClWalkS" as "ClWalkS";
 latexdef "ClWalkS" as "{\rm ClWalkS}";
htmldef "CircuitS" as "CircuitS";
  althtmldef "CircuitS" as "CircuitS";
  latexdef "CircuitS" as " {\rm CircuitS} ";
htmldef "CycleS" as "CycleS";
  althtmldef "CycleS" as "CycleS";
  latexdef "CycleS" as " {\rm CycleS} ";
htmldef "WWalkS" as ' WWalkS ';
  althtmldef "WWalkS" as ' WWalkS ';
  latexdef "WWalkS" as "{\rm WWalkS}";
htmldef "WWalkSN" as ' WWalkSN ';
  althtmldef "WWalkSN" as ' WWalkSN ';
  latexdef "WWalkSN" as "{\rm WWalkSN}";
htmldef "ClWWalkS" as ' ClWWalkS ';
 althtmldef "ClWWalkS" as ' ClWWalkS ';
 latexdef "ClWWalkS" as "{\rm ClWWalkS}";
htmldef "ClWWalkSN" as ' ClWWalkSN ';
 althtmldef "ClWWalkSN" as ' ClWWalkSN ';
 latexdef "ClWWalkSN" as "{\rm ClWWalkSN}";
htmldef "ConnGraph" as "ConnGraph";
 althtmldef "ConnGraph" as "ConnGraph";
 latexdef "ConnGraph" as "{\rm ConnGraph}";
htmldef "EulerPaths" as "EulerPaths";
  althtmldef "EulerPaths" as "EulerPaths";
  latexdef "EulerPaths" as " {\rm EulerPaths} ";
htmldef "FriendGraph" as ' FriendGraph ';
 althtmldef "FriendGraph" as ' FriendGraph ';
 latexdef "FriendGraph" as "{\rm FriendGraph}";
/* Graph theory (old) */
htmldef "VtxALTV" as ' VtxALTV ';
 althtmldef "VtxALTV" as ' VtxALTV ';
 latexdef "VtxALTV" as "{\rm VtxALTV}";
htmldef "UHGraphALTV" as ' UHGraphALTV ';
 althtmldef "UHGraphALTV" as ' UHGraphALTV ';
 latexdef "UHGraphALTV" as "{\rm UHGraphALTV}";
htmldef "USHGraphALTV" as ' USHGraphALTV ';
 althtmldef "USHGraphALTV" as ' USHGraphALTV ';
 latexdef "USHGraphALTV" as "{\rm USHGraphALTV}";
htmldef "GrOrder" as ' GrOrder ';
 althtmldef "GrOrder" as ' GrOrder ';
 latexdef "GrOrder" as "{\rm GrOrder}";
htmldef "GrSize" as ' GrSize ';
 althtmldef "GrSize" as ' GrSize ';
 latexdef "GrSize" as "{\rm GrSize}";
htmldef "FinUSGrph" as ' FinUSGrph ';
 althtmldef "FinUSGrph" as ' FinUSGrph ';
 latexdef "FinUSGrph" as "{\rm FinUSGrph}";
/* Monoids (extension)  */
htmldef "MgmHom" as ' MgmHom ';
 althtmldef "MgmHom" as ' MgmHom ';
 latexdef "MgmHom" as "{\rm MgmHom}";
htmldef "SubMgm" as 'SubMgm';
 althtmldef "SubMgm" as 'SubMgm';
 latexdef "SubMgm" as "{\rm SubMgm}";
htmldef "clLaw" as ' clLaw ';
 althtmldef "clLaw" as ' clLaw ';
 latexdef "clLaw" as "{\rm clLaw}";
htmldef "assLaw" as ' assLaw ';
 althtmldef "assLaw" as ' assLaw ';
 latexdef "assLaw" as "{\rm assLaw}";
htmldef "comLaw" as ' comLaw ';
 althtmldef "comLaw" as ' comLaw ';
 latexdef "comLaw" as "{\rm comLaw}";
htmldef "intOp" as ' intOp ';
 althtmldef "intOp" as ' intOp ';
 latexdef "intOp" as "{\rm intOp}";
htmldef "clIntOp" as ' clIntOp ';
 althtmldef "clIntOp" as ' clIntOp ';
 latexdef "clIntOp" as "{\rm clIntOp}";
htmldef "assIntOp" as ' assIntOp ';
 althtmldef "assIntOp" as ' assIntOp ';
 latexdef "assIntOp" as "{\rm assIntOp}";
htmldef "MgmALT" as 'MgmALT';
 althtmldef "MgmALT" as 'MgmALT';
 latexdef "MgmALT" as "{\rm MgmALT}";
htmldef "CMgmALT" as 'CMgmALT';
 althtmldef "CMgmALT" as 'CMgmALT';
 latexdef "CMgmALT" as "{\rm CMgmALT}";
htmldef "SGrpALT" as 'SGrpALT';
 althtmldef "SGrpALT" as 'SGrpALT';
 latexdef "SGrpALT" as "{\rm SGrpALT}";
htmldef "CSGrpALT" as 'CSGrpALT';
 althtmldef "CSGrpALT" as 'CSGrpALT';
 latexdef "CSGrpALT" as "{\rm CSGrpALT}";
htmldef "Rng" as 'Rng';
 althtmldef "Rng" as 'Rng';
 latexdef "Rng" as "{\rm Rng}";
htmldef "RngHomo" as ' RngHomo ';
 althtmldef "RngHomo" as ' RngHomo ';
 latexdef "RngHomo" as "{\rm RngHomo}";
htmldef "RngIsom" as ' RngIsom ';
 althtmldef "RngIsom" as ' RngIsom ';
 latexdef "RngIsom" as "{\rm RngIsom}";
htmldef "RngCat" as 'RngCat';
 althtmldef "RngCat" as 'RngCat';
 latexdef "RngCat" as "{\rm RngCat}";
htmldef "RingCat" as 'RingCat';
 althtmldef "RingCat" as 'RingCat';
 latexdef "RingCat" as "{\rm RingCat}";
htmldef "RngCatALTV" as 'RngCatALTV';
 althtmldef "RngCatALTV" as 'RngCatALTV';
 latexdef "RngCatALTV" as "{\rm RngCatALTV}";
htmldef "RingCatALTV" as 'RingCatALTV';
 althtmldef "RingCatALTV" as 'RingCatALTV';
 latexdef "RingCatALTV" as "{\rm RingCatALTV}";
htmldef "m'" as "<IMG SRC='_bnj_mPrime.gif' WIDTH=18 HEIGHT=19" +
    " ALT=" + '"' + " m'" + '"' + " TITLE=" + '"' + "m'" + '"' + ">";
htmldef "n'" as "<IMG SRC='_bnj_nPrime.gif' WIDTH=14 HEIGHT=19" +
    " ALT=" + '"' + " n'" + '"' + " TITLE=" + '"' + "n'" + '"' + ">";
htmldef "o'_" as "<IMG SRC='_bnj_oPrime.gif' WIDTH=12 HEIGHT=19" +
    " ALT=" + '"' + " o'_" + '"' + " TITLE=" + '"' + "o'_" + '"' + ">";
htmldef "p'" as "<IMG SRC='_bnj_pPrime.gif' WIDTH=14 HEIGHT=19" +
    " ALT=" + '"' + " p'" + '"' + " TITLE=" + '"' + "p'" + '"' + ">";
htmldef "q'" as "<IMG SRC='_bnj_qPrime.gif' WIDTH=12 HEIGHT=19" +
    " ALT=" + '"' + " q'" + '"' + " TITLE=" + '"' + "q'" + '"' + ">";
htmldef "r'" as "<IMG SRC='_bnj_rPrime.gif' WIDTH=12 HEIGHT=19" +
    " ALT=" + '"' + " r'" + '"' + " TITLE=" + '"' + "r'" + '"' + ">";
htmldef "s'_" as "<IMG SRC='_bnj_sPrime.gif' WIDTH=11 HEIGHT=19" +
    " ALT=" + '"' + " s'" + '"' + " TITLE=" + '"' + "s'" + '"' + ">";
htmldef "t'" as "<IMG SRC='_bnj_tPrime.gif' WIDTH=11 HEIGHT=19" +
    " ALT=" + '"' + " t'" + '"' + " TITLE=" + '"' + "t'" + '"' + ">";
htmldef "u'" as "<IMG SRC='_bnj_uPrime.gif' WIDTH=14 HEIGHT=19" +
    " ALT=" + '"' + " u'" + '"' + " TITLE=" + '"' + "u'" + '"' + ">";
htmldef "v'_" as "<IMG SRC='_bnj_vPrime.gif' WIDTH=13 HEIGHT=19" +
    " ALT=" + '"' + " v'" + '"' + " TITLE=" + '"' + "v'" + '"' + ">";
htmldef "w'" as "<IMG SRC='_bnj_wPrime.gif' WIDTH=16 HEIGHT=19" +
    " ALT=" + '"' + " w'" + '"' + " TITLE=" + '"' + "w'" + '"' + ">";
htmldef "x'" as "<IMG SRC='_bnj_xPrime.gif' WIDTH=14 HEIGHT=19" +
    " ALT=" + '"' + " x'" + '"' + " TITLE=" + '"' + "x'" + '"' + ">";
htmldef "y'" as "<IMG SRC='_bnj_yPrime.gif' WIDTH=13 HEIGHT=19" +
    " ALT=" + '"' + " y'" + '"' + " TITLE=" + '"' + "y'" + '"' + ">";
htmldef "z'" as "<IMG SRC='_bnj_zPrime.gif' WIDTH=13 HEIGHT=19" +
    " ALT=" + '"' + " z'" + '"' + " TITLE=" + '"' + "z'" + '"' + ">";
althtmldef "a'" as
    '<SPAN CLASS=setvar STYLE="color:red">&#x1D44E;&prime;</SPAN>';
althtmldef "b'" as
    '<SPAN CLASS=setvar STYLE="color:red">&#x1D44F;&prime;</SPAN>';
althtmldef "c'" as
    '<SPAN CLASS=setvar STYLE="color:red">&#x1D450;&prime;</SPAN>';
althtmldef "d'" as
    '<SPAN CLASS=setvar STYLE="color:red">&#x1D451;&prime;</SPAN>';
althtmldef "e'" as
    '<SPAN CLASS=setvar STYLE="color:red">&#x1D452;&prime;</SPAN>';
althtmldef "f'" as
    '<SPAN CLASS=setvar STYLE="color:red">&#x1D453;&prime;</SPAN>';
althtmldef "g'" as
    '<SPAN CLASS=setvar STYLE="color:red">&#x1D454;&prime;</SPAN>';
althtmldef "h'" as
    '<SPAN CLASS=setvar STYLE="color:red">&#x210E;&prime;</SPAN>';
althtmldef "t1"  as
    '<SPAN CLASS=setvar STYLE="color:red">&#x1D461;<SUB>1</SUB></SPAN>';
althtmldef "u1"  as
    '<SPAN CLASS=setvar STYLE="color:red">&#x1D462;<SUB>1</SUB></SPAN>';
althtmldef "v1"  as
    '<SPAN CLASS=setvar STYLE="color:red">&#x1D463;<SUB>1</SUB></SPAN>';
althtmldef "w1"  as
    '<SPAN CLASS=setvar STYLE="color:red">&#x1D464;<SUB>1</SUB></SPAN>';
althtmldef "x1"  as
    '<SPAN CLASS=setvar STYLE="color:red">&#x1D465;<SUB>1</SUB></SPAN>';
althtmldef "y1"  as
    '<SPAN CLASS=setvar STYLE="color:red">&#x1D466;<SUB>1</SUB></SPAN>';
althtmldef "z1"  as
    '<SPAN CLASS=setvar STYLE="color:red">&#x1D467;<SUB>1</SUB></SPAN>';
latexdef "a1_"  as "a_1";
latexdef "b1_"  as "b_1";
latexdef "c1_" as "c_1";
latexdef "d1"  as "d_1";
althtmldef "M'" as
    '<SPAN CLASS=class STYLE="color:#C3C">&#x1D440;&prime;</SPAN>';
althtmldef "Y'" as
    '<SPAN CLASS=class STYLE="color:#C3C">&#x1D44C;&prime;</SPAN>';
althtmldef "Z'" as
    '<SPAN CLASS=class STYLE="color:#C3C">&#x1D44D;&prime;</SPAN>';
latexdef "A'" as "A'";
latexdef "B'" as "B'";
latexdef "C'" as "C'";
latexdef "D'" as "D'";
latexdef "E'" as "E'";
latexdef "F'" as "F'";
latexdef "G'" as "G'";
latexdef "H'" as "H'";
latexdef "I'" as "I'";
latexdef "J'" as "J'";
latexdef "K'" as "K'";
latexdef "L'" as "L'";
latexdef "M'" as "M'";
latexdef "N'" as "N'";
latexdef "O'" as "O'";
latexdef "P'" as "P'";
latexdef "Q'" as "Q'";
latexdef "R'" as "R'";
latexdef "S'" as "S'";
latexdef "T'" as "T'";
latexdef "U'" as "U'";
latexdef "V'" as "V'";
latexdef "W'" as "W'";
latexdef "X'" as "X'";
latexdef "Y'" as "Y'";
latexdef "Z'" as "Z'";
htmldef 'A"' as "<IMG SRC='_bnj_caPrimePrime.gif' WIDTH=18 HEIGHT=19 " +
      " ALT=' A" + '"' + "'TITLE='A" + '"' + "'>";
htmldef 'B"' as "<IMG SRC='_bnj_cbPrimePrime.gif' WIDTH=19 HEIGHT=19 " +
      " ALT=' B" + '"' + "'TITLE='B" + '"' + "'>";

/* Mathbox of Glauco Siliprandi */
htmldef "SAlg" as "SAlg"; althtmldef "SAlg" as "SAlg";
  latexdef "SAlg" as "{\rm SAlg}";
htmldef "SalOn" as "SalOn"; althtmldef "SalOn" as "SalOn";
  latexdef "SalOn" as "{\rm SalOn}";
htmldef "SalGen" as "SalGen"; althtmldef "SalGen" as "SalGen";
  latexdef "SalGen" as "{\rm SalGen}";
htmldef "sum^" as "&Sigma;<SUP><small>^</small></SUP>";
  althtmldef "sum^" as "&Sigma;<SUP><small>^</small></SUP>";
  latexdef "sum^" as "\Sigma^\string^";
htmldef "Meas" as "Meas"; althtmldef "Meas" as "Meas";
  latexdef "Meas" as "{\rm Meas}";
htmldef "OutMeas" as "OutMeas"; althtmldef "OutMeas" as "OutMeas";
  latexdef "OutMeas" as "{\rm OutMeas}";
htmldef "CaraGen" as "CaraGen"; althtmldef "CaraGen" as "CaraGen";
  latexdef "CaraGen" as "{\rm CaraGen}";
htmldef "voln*" as "voln<SUP><small>*</small></SUP>";
  althtmldef "voln*" as "voln*";
  latexdef "voln*" as "{\rm voln*}";
htmldef "voln" as "voln"; althtmldef "voln" as "voln";
  latexdef "voln" as "{\rm voln}";
/* End of Glauco Siliprandi's mathbox */

/* End of typesetting definition section */
$)

