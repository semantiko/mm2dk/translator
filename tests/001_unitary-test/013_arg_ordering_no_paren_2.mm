$c -AB-> -BA-> A B -C-> C $.
$v vA vB vC $.

AvA $f A vA $.
BvB $f B vB $.
CvC $f C vC $.

ax-AB $a A -AB-> vA vB $.
ax-BA $a B -BA-> vB vA $.
ax-C  $a C -C-> vB vC vA $.

arrow $p C -C-> -BA-> vB vA vC -AB-> -AB-> vA vB -BA-> vB vA 
  $= AvA BvB ax-AB AvA BvB ax-BA ax-AB AvA BvB ax-BA CvC ax-C $.
