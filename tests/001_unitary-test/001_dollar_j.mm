$( $j
  syntax 'wff';
  syntax '|-' as 'wff';
  unambiguous 'klr 5';
$)

$( $j primitive 'wn' 'wi'; $)

$c 0 A $.
$v x $.

type-x $f A x $.

type-zero $a A 0 $.

$( $j justification 'bijust' for 'df-bi'; $)

$( $j
  equality 'wb' from 'biid' 'bicomi' 'bitri';
  definition 'dfbi1' for 'wb';
$)

$( $j congruence 'notbii'; $)

$( $j syntax 'setvar'; $)

$( $j equality 'wceq' from 'eqid' 'eqcomi' 'eqtri'; $)
