#! /bin/bash -e

tests_folder=tests
tool_script=main
lp_root=lp_root # Racine par défaut utilisée pour tous les fichiers traduits.
                # Nom à changer également dans les fonctions d'import de la
                # traduction.
extension=$(if [ $# = 1 ]; then echo "$1" ; else echo "lp" ;fi)

# Some colors
cyanfonce='\e[0;36m'
neutre='\e[0;m'

  #######################################################################
  #    usage: ./gen_tests dk [one-test] ou ./gen_tests lp [one-test]    #
  #                                                                     #
  # Ce script a pour objectif de traduire les fichiers de tests en      # 
  # Dedukti.                                                            #
  #                                                                     #
  # Ces fichiers sont supposés être dans "tests_folder", lui-même       #
  # composé de dossiers dont le nom suit la nomenclature suivante :     #
  #   "nb_name" où "nb" est un nombre à 3 chiffres, et                  #
  #                "name" est un nom quelconque.                        #
  # Des noms de sous-dossiers acceptables sont donc par exemple,        #
  # 001_max ou encore 010_Matching_Logic.                               #
  #                                                                     #
  # Chacun de ces sous-dossiers contient des fichiers dont le nom       #
  # suit la même nomenclature que précédemment.                         #
  #                                                                     #
  # La hiérarchie précédemment décrite est conservée lors de la         #
  # traduction en Dedukti. Toutes les extensions des fichiers           #
  # deviennent ".dk" ou ".lp" en fonction de l'option passée par la     #
  # ligne de commande.                                                  #
  # Ainsi, les duals du dossier "tests_folder" sont "dk-generated" et   #
  # et "lp-generated", et se trouvent dans "../tests_folder".           #
  #                                                                     #
  #######################################################################

nb_nomencla=5 # Nombre permettant de couper avec "cut" pour supprimer les chiffres de la nomenclature

# Création du dossier où seront les fichiers générés, 
# sans message d'erreur si le dossier existe déjà
gen_folder=$(if [ "$extension" = "dk" ]; then echo "dk-generated" ; else echo "lp-generated" ;fi)
rm -rf $gen_folder
mkdir $gen_folder

# Création du fichier de management de fichiers pour LP, si besoin
LPpkg="lambdapi.pkg"
if [ "$extension" = "lp" ]; then
  touch $LPpkg
  echo "package_name = root" >  $LPpkg ;
  echo "root_path    = root" >> $LPpkg ;
  mv $LPpkg $gen_folder;fi


cd $tests_folder
for_test=$(if [ "$#" = 2 ];
    then echo $(find . -mindepth 1 -maxdepth 1 -type d -iname "$2" | sort -d | cut -c3-) ;
    else echo $(find . -mindepth 1 -maxdepth 1 -type d | sort -d | cut -c3-) ;
    fi)

# Itération sur chaque dossier présent dans "tests_folder"
for curr_folder in $for_test; do
  if [ $(echo $curr_folder | cut -c-4) = "000_" ]
  then continue
  else
   cd $curr_folder ; echo ""
   echo -e "${cyanfonce}Translation of the folder:${neutre}" $curr_folder

   # Création du dual du sous-dossier courant
   mkdir ../../$gen_folder/$curr_folder

   # Traduction vers Dedukti des fichiers se trouvant dans "curr_folder"
   for f in $(find . -mindepth 1 -type f | sort -d | cut -c3-); do
     if [ ${f#*.} = "mm" ]
     then
       echo -e "${cyanfonce}Checking by Metamath of the file:${neutre}" $f
       cd ../..
       ./metamath "read '$tests_folder/$curr_folder/$f'" 'verify proof *' exit
       echo -e "${cyanfonce}Translation of the file:${neutre}" $f
       new_name=${f%.*} # Suppression de l'extension
       filename=$(echo -e $new_name"."$extension)
       ./$tool_script $tests_folder/$curr_folder/$f # >> $filename
       mv $tests_folder/$curr_folder/$filename $gen_folder/$curr_folder
       cd $tests_folder/$curr_folder
     else continue
     fi
   done

   # Lambdapi check
   cd ../../$gen_folder/$curr_folder
   echo -e "${cyanfonce}Beginning of Lambdapi check..." $curr_folder
   for f in $(find . -mindepth 1 -maxdepth 1 -type f | sort -d | cut -c3-); do
     echo -e "${cyanfonce}Checking the file " $f
     lambdapi check --no-warnings -v 0 $f
     if [ ! -s $generated_file ] ; then
        echo "$generated_file existe mais est vide"
        exit 1
   fi
   done
   echo -e "${cyanfonce}...ending of Lambdapi check."
   cd ../../$tests_folder
  fi
done
