#! /bin/bash -e

touch lambdapi.pkg
echo "package_name = root" >  lambdapi.pkg
echo "root_path    = root" >> lambdapi.pkg

#path=tests/001_unitary-test/001_dollar_j
#test_file=$path.mm
#generated_file=$path.lp
#./metamath "read '$test_file'" 'verify proof *' exit
#./main $test_file
#if [ ! -s $generated_file ] ; then
#    echo "$generated_file existe mais est vide"
#    exit 1
#fi
#lambdapi check $generated_file
#echo ""
#echo ">>>>>> \$j est parsé ! :) <<<<<<<"
#echo ""

#path=tests/001_unitary-test/002_dollar_t
#test_file=$path.mm
#generated_file=$path.lp
#./metamath "read '$test_file'" 'verify proof *' exit
#./main $test_file
#if [ ! -s $generated_file ] ; then
#    echo "$generated_file existe mais est vide"
#    exit 1
#fi
#lambdapi check $generated_file
#echo ""
#echo ">>>>>> \$t est parsé ! :) <<<<<<<"
#echo ""

path=tests/001_unitary-test/010_argument_ordering
test_file=$path.mm
generated_file=$path.lp
./metamath "read '$test_file'" 'verify proof *' exit
./main $test_file
if [ ! -s $generated_file ] ; then
    echo "$generated_file existe mais est vide"
    exit 1
fi
lambdapi check $generated_file
echo ""
echo ">>>>>> Mini proof OK ! :) <<<<<<<"
echo ""

#path=tests/001_unitary-test/011_no_parenthesis
#test_file=$path.mm
#generated_file=$path.lp
#./metamath "read '$test_file'" 'verify proof *' exit
#./main $test_file
#if [ ! -s $generated_file ] ; then
#    echo "$generated_file existe mais est vide"
#    exit 1
#fi
#lambdapi check $generated_file
#echo ""
#echo ">>>>>> no ( ) OK ! :) <<<<<<<"
#echo ""

#path=tests/001_unitary-test/012_arg_ordering_no_paren_1
#test_file=$path.mm
#generated_file=$path.lp
#./metamath "read '$test_file'" 'verify proof *' exit
#./main $test_file
#if [ ! -s $generated_file ] ; then
#    echo "$generated_file existe mais est vide"
#    exit 1
#fi
#lambdapi check $generated_file
#echo ""
#echo ">>>>>> order + no ( ) - (1) OK ! :) <<<<<<<"
#echo ""

#path=tests/001_unitary-test/013_arg_ordering_no_paren_2
#test_file=$path.mm
#generated_file=$path.lp
#./metamath "read '$test_file'" 'verify proof *' exit
#./main $test_file
#if [ ! -s $generated_file ] ; then
#    echo "$generated_file existe mais est vide"
#    exit 1
#fi
#lambdapi check $generated_file
#echo ""
#echo ">>>>>> order + no ( ) - (2) OK ! :) <<<<<<<"
#echo ""

path=tests/002_easy-logic/001_anatomy
test_file=$path.mm
generated_file=$path.lp
./metamath "read '$test_file'" 'verify proof *' exit
./main $test_file
if [ ! -s $generated_file ] ; then
    echo "$generated_file existe mais est vide"
    exit 1
fi
lambdapi check $generated_file
echo ""
echo ">>>>>> Anatomy marche ! :) <<<<<<<"
echo ""

test_file=tests/002_easy-logic/002_mendelson.mm
generated_file=tests/002_easy-logic/002_mendelson.lp
./metamath "read '$test_file'" 'verify proof *' exit
./main $test_file
if [ ! -s $generated_file ] ; then
    echo "$generated_file existe mais est vide"
    exit 1
fi
lambdapi check $generated_file
echo ""
echo ">>>>>> Mendelson marche ! :) <<<<<<<"
echo ""


test_file=tests/003_hilbert/001_hilbert-1-proof.mm
generated_file=tests/003_hilbert/001_hilbert-1-proof.lp
./metamath "read '$test_file'" 'verify proof *' exit
./main $test_file
if [ ! -s $generated_file ] ; then
    echo "$generated_file existe mais est vide"
    exit 1
fi
lambdapi check $generated_file
echo ""
echo ">>>>>> Mini Hilbert marche ! :) <<<<<<<"
echo ""

test_file=tests/003_hilbert/002_hilbert-101-proof.mm
generated_file=tests/003_hilbert/002_hilbert-101-proof.lp
./metamath "read '$test_file'" 'verify proof *' exit
./main $test_file
if [ ! -s $generated_file ] ; then
    echo "$generated_file existe mais est vide"
    exit 1
fi
lambdapi check $generated_file
echo ""
echo ">>>>>> Hilbert 101 marche ! :) <<<<<<<"
echo ""

test_file=tests/003_hilbert/003_hilbert-979-proof.mm
generated_file=tests/003_hilbert/003_hilbert-979-proof.lp
./metamath "read '$test_file'" 'verify proof *' exit
./main $test_file
if [ ! -s $generated_file ] ; then
    echo "$generated_file existe mais est vide"
    exit 1
fi
lambdapi check $generated_file
echo ""
echo ">>>>>> Hilbert 979 marche ! :) <<<<<<<"
echo ""
