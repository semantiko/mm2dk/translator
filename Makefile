all:
	dune build src/main.exe
	cp ./_build/default/src/main.exe ./main

install:
	#opam install --yes lambdapi
	opam pin add --yes git+https://gitlab.com/semantiko/DiaLeKto
	opam install --yes yojson
	eval $(opam env)

metamath:
	bash -e utilities/get_metamath-exe.sh

test:
	make metamath
	bash -e tests/mini_tests.sh

local-test:
	bash -e tests/gen_tests.sh lp

full-test:
	make test
	bash -e utilities/test_examples.sh
	cd examples && make test

clean:
	rm -f src/parsing/MM_lexer.ml src/parsing/MM_parser.ml \
		  src/parsing/MM_parser.mli \
          *.cmo *.cmi main metamath lambdapi.pkg
	rm -rf _build lp-generated dk-generated examples metamath-exe

full-clean:
	make clean
	rm -f *.mm *.json *.lp *.dk
